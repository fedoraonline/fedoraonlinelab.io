---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18 Alpha ritarda ancora: 3 settimane!'
created: 1346963338
---
<p>
	Come da previsioni la Alpha di Fedora, e di conseguenza anche la versione finale, non arriver&agrave; con un ritardo inferiore alle 3 settimane. Cos&igrave; &egrave; stato deciso al meeting di stasera, e c&#39;&egrave; la possibilit&agrave; che non sia finita qui, anche se qualcosa si muove.</p>
<p>
	Il nuovo installer Ananconda sta dando pi&ugrave; problemi di quanto si pensava, ma gli sviluppatori sono fiduciosi di riuscire a sciogliere anche gli ultimi nodi bloccanti. I blocker bugs [1] sono meno delle matrici non soddisfatte per il rilascio della Alpha [2] [3] [4], per chi vuole pu&ograve; leggersi tutto il log qui [5].</p>
<p>
	[1] <a href="http://qa.fedoraproject.org/blockerbugs/current" target="_blank">http://qa.fedoraproject.org/<wbr />blockerbugs/current</a><br />
	[2] <a href="https://fedoraproject.org/wiki/Test_Results:Current_Base_Test" target="_blank">https://fedoraproject.org/<wbr />wiki/Test_Results:Current_<wbr />Base_Test</a><br />
	[3] <a href="http://fedoraproject.org/wiki/Test_Results:Current_Installation_Test" target="_blank">http://fedoraproject.org/wiki/<wbr />Test_Results:Current_<wbr />Installation_Test</a><br />
	[4] <a href="http://fedoraproject.org/wiki/Test_Results:Current_Desktop_Test" target="_blank">http://fedoraproject.org/wiki/<wbr />Test_Results:Current_Desktop_<wbr />Test</a><br />
	[5] <a href="http://bit.ly/Rf9AfR" target="_blank">http://bit.ly/Rf9AfR</a></p>
<p>
	&nbsp;</p>
