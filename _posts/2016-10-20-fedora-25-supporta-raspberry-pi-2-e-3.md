---
categories:
- staff news
layout: ultime_news
title: Fedora 25 supporta Raspberry Pi (2 e 3)!
created: 1476991634
---
<p class="rtejustify"><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2016/10/raspi3-f25beta-945x400.jpg" style="width: 400px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><em>Il presente articolo &egrave; un adattamento in italiano dell&#39;originale, scritto da <a href="https://fedoraproject.org/wiki/User:Pbrobinson">Peter Robinson</a> ed apparso recentemente su <a href="https://fedoramagazine.org/raspberry-pi-support-fedora-25-beta/">Fedora Magazine</a>. Peter &egrave; membro del team Fedora Release Engineering ed ha lavorato per diversi anni al processo di rilascio generale e al supporto hardware per architetture alternative, come ARM e Power. Egli annuncia grandi novita per i possessori di Raspberry Pi. All&#39;interno di Fedora Online, &egrave; stato aperto un <a href="http://forum.fedoraonline.it/viewtopic.php?id=24644">topic</a> dedicato per discutere in merito alla notizia.</em></p>
<p class="rtejustify"><br />
	Il supporto di Rasberry Pi in Fedora era una caratteristica voluta da molto: finalmente l&#39;abbiamo raggiunta, appena in tempo per la Beta di Fedora 25!<br />
	La domanda che pi&ugrave; mi &egrave; stata sottoposta per un gran numero di anni, riguarda infatti il supporto nativo per questa scheda. &Egrave; inoltre una sfida su cui ho lavorato per un periodo decisamente lungo, durante il mio tempo libero. I lettori pi&ugrave; esperti si saranno accorti che avevamo quasi raggiunto l&#39;obiettivo per Fedora 24; ho per&ograve; rinunciato all&#39;idea di un annuncio ufficiale, perch&eacute; avevo la sensazione che l&#39;esperienza utente non fosse sufficientemente buona. Erano presenti dei problemi forse minori, ma che comunque inficiavano la facilit&agrave; d&#39;uso.<br />
	<br />
	<br />
	<strong>Perch&eacute; &egrave; stato necessario impiegare cos&igrave; tanto tempo?</strong><br />
	Fondamentalmente, le esigenze erano quattro:<br />
	1) Supporto hardware decente da parte del kernel/userspace dell&#39;upstream, senza il bisogno di driver binari aggiuntivi.<br />
	2) Possibilit&agrave; di redistribuire il firmware.<br />
	3) Possibilit&agrave; di includere il supporto in Fedora ARM, senza aggiungere un grande carico di lavoro extra in capo al piccolo gruppo che supporta/testa questa architettura in Fedora.<br />
	4) Il progetto &egrave; nato come un passatempo: come gi&agrave; successo, abbiamo preferito riporre fiducia nella realizzazione di drivers open e supporto kernel da parte dell&#39;upstream. Tutto questo &egrave; stato fuori dal nostro controllo, richiedendo quindi il giusto tempo necessario.</p>
<p class="rtejustify"><br />
	<strong>Cos&#39;&egrave; supportato?</strong><br />
	Supportiamo qualsiasi cosa che l&#39;utente si aspetterebbe da un dispositivo riconosciuto da Fedora. Abbiamo una corretta gestione userspace e kernel, con tutte le caratteristiche standard, come l&#39;esecuzione di SELinux. Si riceve l&#39;usuale insieme di aggiornamenti e non esiste il bisogno di escludere il kernel dalla lista degli updates! Quest&#39;ultimo, supporta una grande moltitudine di driver, come vari adattatori USB WiFi, ecc. &Egrave; possibile eseguire l&#39;ambiente desktop desiderato (si veda pi&ugrave; avanti, a riguardo di essi) o Docker/Kubernetes/Ceph/Gluster come gruppo di dispositivi - tranne per il fatto che questo procedimento risulta lento, avendo a disposizione un singolo cavo USB condiviso -.</p>
<p class="rtejustify"><br />
	<strong>Raspberry Pi 2</strong><br />
	Per quanto riguarda Raspberry Pi 2, il supporto &egrave; piuttosto buono. Ho testato l&#39;installazione minimale, Workstation e XFCE. MMC, USB e interfaccia di rete sono disponibili. Possiamo affermare la stessa cosa, per quanto riguarda altri componenti e l&#39;accelerazione grafica (gestita da un driver Open Source). Per raggiungere i migliori risultati, &egrave; comunque consigliabile acquistare una scheda micro SD di classe 10, di elevata qualit&agrave;.</p>
<p class="rtejustify"><br />
	<strong>Raspberry Pi 3</strong><br />
	Il supporto dell&#39;hardware di Raspberry Pi 3, &egrave; assai similare a quanto raggiunto per i componenti standard di Raspberry Pi 2. Attualmente, non supportiamo l&#39;adattatore WiFi/Bluetooth incorporato; al momento quindi, RPi3 si configura come una &quot;versione pi&ugrave; veloce&quot; di RPi2. Stiamo continuamente lavorando, in modo da abilitare presto il WiFi, dato che il firmware &egrave; ora distribuibile. Troviamo poi alcune ulteriori perplessit&agrave;, che per&ograve; sono ben documentate all&#39;interno del wiki di supporto fornito ufficialmente dall&#39;<a href="https://github.com/anholt/linux/wiki/Raspberry-Pi-3">upstream</a>.</p>
<p class="rtejustify">Attualmente, concepiamo Raspberry 3 come dispositivo ARMv7, similarmente a quanto accade per la Raspberry Pi Foundation. Sono comunque conscio del fatto che il supporto aarch64 sia ora fornito dall&#39;upstream ed, eventualmente, saremmo in grado di supportarlo anche noi. Per fare ci&ograve; per&ograve;, &egrave; richiesto del lavoro extra, per raggiungere un buon livello qualitativo. Il rilascio di immagini aggiuntive per supportare l&#39;architettura 64bit inoltre, non genera vantaggi tangibili, in quanto Raspberry Pi 3 ha comunque la limitazione di un solo Giga-Byte di memoria RAM.</p>
<p class="rtejustify"><br />
	<strong>Cosa non &egrave; supportato (per ora!)</strong><br />
	Lo stato dei vari componenti, come WiFi, decodificazione di contenuti multimediali e supporto HAT, &egrave; ben documentato nella sezione FAQ (domande frequenti) dell&#39;apposita pagina <a href="https://fedoraproject.org/wiki/Raspberry_Pi#Frequently_Asked_Questions)">wiki del Fedora Project</a>. Essa verr&agrave; costantemente aggiornata, nel caso di cambiamenti nella gestione dei vari componenti.</p>
<p class="rtejustify"><br />
	<strong>Cosa non sar&agrave; mai supportato</strong><br />
	In sostanza, si tratta dei pi&ugrave; vecchi Raspberry Pi, che montano processori ARMv6. Al momento, mi riferisco ai modelli Zero, model A, la prima versione del model B e la generazione corrente di prodotti &quot;compute module&quot;. In questi casi, come alternativa, si consiglia l&#39;utilizzo di <a href="https://pignus.computer/">Pignus</a> (preciso per&ograve; che io non ho testato tale sistema operativo).<br />
	Supporteremo il futuro modulo &quot;compute 3&quot;, appena sar&agrave; reso disponibile. Similarmente, il supporto riguardante qualsiasi nuovo dispositivo, verr&agrave; preso in considerazione appena le necessarie informazioni saranno fruibili.</p>
<p class="rtejustify"><br />
	<strong>Come iniziare</strong><br />
	Mi sono dilungato troppo? Facciamo partire Fedora!<br />
	<br />
	I passaggi per Linux, Windows e MacOS, necessari alla creazione di una card SD, sono delineati all&#39;interno della pagina wiki di Fedora (https://fedoraproject.org/wiki/Raspberry_Pi#Preparing_the_SD_card), dedicata al Raspberry Pi. &Egrave; possibile reperire anche dettagli relativi alle immagini pi&ugrave; recenti, ma allego qui i link diretti per quanto riguarda la beta di Fedora 25:<br />
	- <a href="https://download.fedoraproject.org/pub/fedora/linux/releases/test/25_Beta/Workstation/armhfp/images/Fedora-Workstation-armhfp-25_Beta-1.1-sda.raw.xz">Workstation</a><br />
	- <a href="https://download.fedoraproject.org/pub/fedora/linux/releases/test/25_Beta/Server/armhfp/images/Fedora-Server-armhfp-25_Beta-1.1-sda.raw.xz">Server</a><br />
	- <a href="https://download.fedoraproject.org/pub/fedora/linux/releases/test/25_Beta/Spins/armhfp/images/">Spins e immagine minimale</a><br />
	<br />
	Per applicare l&#39;immagine alla scheda, &egrave; possibile impartire il seguente comando all&#39;interno di un emulatore di terminale (occorre adeguare il nome del dispositivo e dell&#39;immagine in base al proprio sistema!)<br />
	<br />
	<u><em><strong>$ xzcat Fedora-IMAGE-NAME.raw.xz | sudo dd status=progress bs=4M of=/dev/XXX</strong></em></u><br />
	<br />
	Esistono ulteriori opzioni, comunque documentate all&#39;interno della wiki. Si rende inoltre necessario il ridimensionamento del filesystem di root (inizialmente &egrave; ristretto, per ridurre la dimensione del download). La via pi&ugrave; facile, consiste nell&#39;operare con gparted prima di inserire la scheda all&#39;interno del Raspberry Pi.<br />
	<br />
	Al primo avvio, &egrave; possibile trovare la schermata fornita dal software initial-setup, che consente di creare un profilo utente. Per ragioni di sicurezza, analogamente a quanto accade per le ISO Live delle architetture x86, non pre-configuriamo account e password predefiniti.</p>
<p class="rtejustify"><br />
	<strong>Come ricevere aiuto</strong><br />
	<br />
	Dov&#39;&egrave; possibile trovare aiuto in caso di necessit&agrave;? I consueti forum di supporto per Fedora sono questi:<br />
	<br />
	- <a href="https://ask.fedoraproject.org/">Ask Fedora</a><br />
	- <a href="https://lists.fedoraproject.org/admin/lists/arm%40lists.fedoraproject.org/">Mailing list Fedora ARM</a><br />
	- <a href="https://fedoraproject.org/wiki/IRC">Canale IRC #fedora-arm su Freenode</a><br />
	&nbsp;</p>
