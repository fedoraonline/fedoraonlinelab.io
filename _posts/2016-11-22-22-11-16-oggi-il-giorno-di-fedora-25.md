---
categories:
- staff news
layout: ultime_news
title: '22/11/16: Oggi è il giorno di Fedora 25!'
created: 1479796008
---
<p><img alt="" src="http://qalab.altervista.org/folnews/f25.png" style="width: 399px; height: 169px;" /></p>
<p>Nel corso della giornata di oggi, sar&agrave; ufficilamente rilasciata la versione 25 di Fedora.</p>
<p>I link per il download, sono i seguenti:<br />
	- <a href="https://getfedora.org/workstation/">https://getfedora.org/workstation/</a><br />
	- <a href="https://getfedora.org/server/">https://getfedora.org/server/</a><br />
	- <a href="https://getfedora.org/cloud/">https://getfedora.org/cloud/</a><br />
	- <a href="https://spins.fedoraproject.org/">https://spins.fedoraproject.org/</a><br />
	- <a href="https://labs.fedoraproject.org/">https://labs.fedoraproject.org/</a><br />
	- <a href="https://arm.fedoraproject.org/">https://arm.fedoraproject.org/</a><br />
	<br />
	La wiki di Fedora Online contiene alcune utili guide per procedere all&#39;installazione o all&#39;avanzamento di versione:<br />
	- Nuova installazione (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>)<br />
	- Upgrade da rilasci precedenti (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>)<br />
	<br />
	Di seguito si propone un adattamento italiano di un <a href="https://fedoramagazine.org/whats-new-fedora-25-workstation/">recente articolo</a> di <strong>Paul W. Frields</strong>, esaminando alcune delle principali novit&agrave;, che accompagnano il nuovo rilascio dell&#39;edizione Workstation.</p>
<p><strong>Introduzione</strong><br />
	La versione 25 Workstation rappresenta il pi&ugrave; recente rilascio dell&#39;innovativo sistema operativo libero chiamato Fedora. Dalla giornata del 22 Novembre 2016, &egrave; possibile scaricare l&#39;edizione in questione, raggiungendo questo (https://getfedora.org/workstation) link.<br />
	<br />
	<br />
	<strong>GNOME</strong> <strong>3.22</strong><br />
	L&#39;ambiente desktop predefinito &egrave; il risultato degli sforzi della GNOME community (https://gnome.org/). I programmatori hanno svolto un lavoro notevole per realizzare il nuovissimo rilascio 3.22. Alcuni dei miglioramenti sono i seguenti:<br />
	- Possibilit&agrave; di rinominare congiuntamente diversi file, all&#39;interno dell&#39;applicazione Gnome Files.<br />
	- Integrazione delle funzionalit&agrave; di compressione all&#39;interno di Gnome Files.<br />
	- Presenza di un nuovo strumento per la configurazione della tastiera.<br />
	- Esistenza di una nuova pagina principale, all&#39;interno dell&#39;applicazione Gnome Software.<br />
	- Migliorata la navigazione per categorie, all&#39;interno di Gnome Software.<br />
	- Possibilit&agrave; di variare la velocit&agrave; di playback, all&#39;interno dell&#39;applicazione Gnome Videos.<br />
	- e... <a href="https://help.gnome.org/misc/release-notes/3.22/">Molto altro</a>!<br />
	<br />
	<br />
	<strong>Display server Wayland</strong><br />
	Di default, Wayland (https://wayland.freedesktop.org/) rimpiazza il vecchio server grafico X11. L&#39;obbiettivo &egrave; quello di fornire un&#39;esperienza utente pi&ugrave; ricca e fluida. Come avviene per ogni tipo di software, alcuni bug potrebbero ancora essere presenti. &Egrave; comunque ancora possibile selezionare la vecchia sessione X11, al login. Wayland &egrave; stato costruito in modo da adattarsi perfettamente ai nuovi sistemi operativi e all&#39;hardware pi&ugrave; recente.<br />
	<br />
	<br />
	<strong>Fedora Media Writer</strong><br />
	Il nuovo Fedora Media Writer facilita ulteriormente il download e l&#39;installazione di Fedora Workstation; esso pu&ograve; comunque essere utilizzato anche per altre edizioni, spins, labs. Questo strumento semplifica la ricerca, lo scaricamente del rilascio corrente di Fedora e ne propone la scrittura all&#39;interno di dispositivi rimovibili (come chiavette USB). In questo modo, si ha la possibilit&agrave; di provare il sistema operativo prima dell&#39;installazione vera e propria. Sebbene le Live USB non rappresentino una novit&agrave;, Fedora Media Writer ne semplifica la preparazione e l&#39;utilizzo.<br />
	<br />
	<br />
	<strong>Supporto alla decodifica MP3</strong><br />
	Fedora 25 include un codec per la decodifica di file MP3. Se l&#39;utente tenta di riprodurre questo tipo di brani, GNOME Software propone l&#39;installazione del plugin aggiuntivo.<br />
	<br />
	<br />
	<strong>Supporto Flatpak</strong><br />
	Fedora 25 introduce il supporto ai software Flatpak (https://flatpak.org/) e ne semplifica l&#39;installazione, l&#39;aggiornamento, la rimozione e la pacchettizzazione.<br />
	<br />
	<br />
	<strong>Estensioni</strong><br />
	&Egrave; stato rimosso il controllo di compatibilit&agrave; tra le estensioni (https://extensions.gnome.org/) e la versione di GNOME Shell in uso. Questo test si rendeva necessario durante i primi giorni di GNOME 3, perch&eacute; le interfacce erano in continuo mutamento. Esso non &egrave; quindi ora pi&ugrave; necessario, in quanto l&#39;ambiente ha raggiunto una certa &quot;stabilit&agrave; evolutiva&quot;.<br />
	<br />
	<br />
	<strong>Altro</strong><br />
	Le novit&agrave; descritte, sono solo alcuni dei miglioramenti apportati da Fedora 25. Fedora fornisce l&#39;accesso a migliaia (https://download.fedoraproject.org/pub/fedora/linux/releases/25/Everything/x86_64/os/Packages/) di applicazioni software, gestite dalla comunit&agrave;. Molte di queste sono state aggiornate.<br />
	<br />
	<strong>Bug comuni</strong><br />
	La wiki del Fedora Project, contiene, come di consueto, una pagina relativa ai bug comuni. Il link &egrave; il seguente: <a href="https://fedoraproject.org/wiki/Common_F25_bugs">https://fedoraproject.org/wiki/Common_F25_bugs</a><br />
	<br />
	<strong>Fonti e link ulteriori</strong><br />
	- Fedora Magazine: novit&agrave; Fedora Workstation (<a href="https://fedoramagazine.org/whats-new-fedora-25-workstation/">https://fedoramagazine.org/whats-new-fedora-25-workstation/</a>)<br />
	&nbsp;</p>
