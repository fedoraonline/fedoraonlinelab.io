---
categories:
- staff news
layout: ultime_news
title: Fedora 21 Alpha pronta - rilascio il 23 settembre!
created: 1411065360
---
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Alla fine abbiamo saltato un ciclo di rilascio intero ma dopo tanto lavoro &egrave; fatta. E&#39; iniziato da pochi minuti il vero viaggio verso Fedora 21, che &egrave; conosciuta anche come Fedora-next. La prima tappa &egrave; la versione Alpha, tanto attesa da tutti, che vedr&agrave; la luce tra pochi giorni, ovvero&nbsp;</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;"><strong>marted&igrave; 23 settembre, solito orario delle 10 EST (ore 16 italiane)</strong>.</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Buon testing a tutti!</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Log del meeting:&nbsp;<a href="http://is.gd/OkmgRo">http://is.gd/OkmgRo</a></p>
