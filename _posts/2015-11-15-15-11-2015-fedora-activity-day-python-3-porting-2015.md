---
categories:
- staff news
layout: ultime_news
title: '15/11/2015: Fedora Activity Day - Python 3 Porting 2015'
created: 1447579978
---
<p><img alt="" src="https://communityblog.fedoraproject.org/wp-content/uploads/2015/11/Parselmouth-Badge.png" style="width: 200px; height: 200px;" /></p>
<p>Solo un terzo dei pacchetti Python presenti su Fedora supporta Python 3. Si &egrave; indetto un Fedora Activity Day (FAD) per questo fine settimana per accelerare questo processo di conversione, con l&#39;intenzione di rendere Python 3 l&#39;interprete di default per F24. I partecipanti, riceveranno il badge &quot;Parselmouth&quot;.</p>
<p>Per maggiori informazioni, &egrave; possibile consultare:</p>
<ul>
	<li>
		Topic dell&#39;ambassador Frafra su Fedora Online: <a href="http://forum.fedoraonline.it/viewtopic.php?id=24022">http://forum.fedoraonline.it/viewtopic.php?id=24022</a></li>
	<li>
		Articolo Fedora Magazine: <a href="https://fedoramagazine.org/help-port-python-packages-python-3/">https://fedoramagazine.org/help-port-python-packages-python-3/</a></li>
</ul>
