---
categories:
- staff news
layout: ultime_news
title: FUDCon Cordoba 2015
created: 1442675634
---
<p><img alt="" src="https://fudcon.fedoraproject.org/static/images/logo_cordoba_2015.jpg" style="width: 203px; height: 137px; float: left; margin-left: 5px; margin-right: 5px;" />Si &egrave; da poco concluso il <strong>FUDCon</strong> (Fedora Users and Developers Conference), importante evento nel mondo del software libero che si svolge in diverse parti del mondo, di solito una volta all&#39;anno per regione.<br />
	<br />
	Il FUDCon &egrave; una combinazione di sessioni, conferenze, workshop e hackfest in cui i partecipanti al progetto possono lavorare su iniziative specifiche. Tra gli argomenti: infrastrutture, sviluppo di features, costruzione e/o il consolidamento della comunit&agrave;, gestione e governance dei progetti, marketing, collaudo e controllo qualit&agrave;, pacchettizzazione, ecc...<br />
	<br />
	La location per il FUDCon della regione LATAM 2015 &egrave; stata quella di C&oacute;rdoba, Argentina, dal 10 al 12 settembre 2015, e in questo importante evento non poteva certo mancare la presenza di un noto ambassador italiano: Robert Mayr (alias Robyduck).<br />
	<br />
	In quattro post del suo blog Robyduck ci racconta la sua esperienza, dove speranzoso di trovare il caldo sudamericano ha invece trovato una piacevole e fresca temperatura di soli 5&deg;C.... (Brrr!)</p>
<p>Giorno0: http://robyduck.fedoraonline.it/?p=148</p>
<p>Giorno1: http://robyduck.fedoraonline.it/?p=154</p>
<p>Giorno2: <a class="ot-anchor aaTEdf" dir="ltr" href="http://robyduck.fedoraonline.it/?p=164" rel="nofollow" target="_blank">http://robyduck.fedoraonline.it/?p=164</a></p>
<p>Giorno3: <a class="ot-anchor aaTEdf" dir="ltr" href="http://robyduck.fedoraonline.it/?p=173" rel="nofollow" target="_blank">http://robyduck.fedoraonline.it/?p=173</a>﻿</p>
<p>&nbsp;</p>
<p>Di seguito un video caricato da Mar&iacute;a Leandro:</p>
<p>https://www.youtube.com/watch?v=rPYX8o-m19s</p>
<p>&nbsp;</p>
<p>Enjoy!</p>
<p>&nbsp;</p>
