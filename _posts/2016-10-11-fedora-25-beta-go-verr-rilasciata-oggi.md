---
categories:
- staff news
layout: ultime_news
title: 'Fedora 25 Beta è GO: verrà rilasciata oggi!'
created: 1476185377
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Nel corso del Go/No-Go Meeting di settimana scorsa, relativo a Fedora 25 Beta, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.</span></p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Fedora 25 Beta sar&agrave; quindi resa pubblicamente disponibile nel corso della giornata odierna.</span></p>
<p><span style="color: rgb(46, 52, 54); font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2016-10-06/f25-beta-go_no_go-meeting.2016-10-06-17.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting-2/2016-10-06/f25-beta-go_no_go-meeting.2016-10-06-17.00.log.html</a></span></p>
<p>&nbsp;</p>
<p><span style="color: rgb(46, 52, 54); font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Salvo nuovi rinvii, il rilascio finale di Fedora 25 vedr&agrave; la luce il giorno 15/11/2016.</span></p>
<p class="rtejustify">&nbsp;</p>
