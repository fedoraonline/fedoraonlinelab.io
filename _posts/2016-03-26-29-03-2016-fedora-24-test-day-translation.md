---
categories:
- staff news
layout: ultime_news
title: '29/03/2016: Fedora 24 Test Day (Translation)'
created: 1458983440
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>Nella giornata di Marted&igrave; 29 Marzo 2016, &egrave; prevista una giornata di testing.</p>
<p>Tale evento, promosso dal Fedora Language Testing Group (<a href="https://fedoraproject.org/wiki/FLTG">https://fedoraproject.org/wiki/FLTG</a>), si focalizzer&agrave; sullo stato delle traduzioni di Fedora 24.</p>
<p>Al momento, non sono presenti maggiori dettagli. La pagina (ancora provvisoria) relativa al TestDay &egrave; disponibile qui: <a href="https://fedoraproject.org/wiki/Test_Day:2016-03-29_Translation_Test_Day">https://fedoraproject.org/wiki/Test_Day:2016-03-29_Translation_Test_Day</a>.</p>
<p>&Egrave; comunque facile prevedere che, uno dei requisiti principali, sar&agrave; il possesso di un sistema Fedora 24 aggiornato, su cui eseguire un certo numero di controlli.</p>
