---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18: versione finale ancora rimandata al 15 gennaio'
created: 1357255691
---
<p>
	Stasera dopo ben 3 ore di meeting &egrave; stato deciso di rimandare l&#39;uscita della versione finale di Fedora 18 di un&#39;altra settimana, ovvero a</p>
<p class="rtecenter">
	<strong>marted&igrave; 15 gennaio 2013</strong></p>
<p>
	La release schedule aggiornata &egrave; disponibile qui:
<a href="https://fedoraproject.org/wiki/Releases/18/Schedule">https://fedoraproject.org/wiki/Releases/18/Schedule</a></p>
