---
categories:
- staff news
layout: ultime_news
title: 'Fedora 24: com''è nato lo sfondo della prossima Alpha'
created: 1458464614
---
<p class="rtejustify"><img alt="" height="201" src="https://mashaleonova.files.wordpress.com/2016/03/24_f.png?w=660" width="268" /></p>
<p class="rtejustify"><a href="https://mashaleonova.wordpress.com/">Masha Leonova</a>, membro del <a href="https://fedoraproject.org/wiki/Design">Fedora Design Team</a>, ha recentemente pubblicato un interessante post.</p>
<p class="rtejustify">L&#39;articolo in questione, riguarda il processo di realizzazione dello sfondo per il prossimo rilascio di Fedora.</p>
<p class="rtejustify">Il link &egrave; il seguente: <a href="https://mashaleonova.wordpress.com/2016/03/14/fedora-24-alpha-wallpaper/">https://mashaleonova.wordpress.com/2016/03/14/fedora-24-alpha-wallpaper/</a></p>
