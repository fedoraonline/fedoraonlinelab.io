---
categories:
- staff news
layout: ultime_news
title: Il rilascio di Fedora 26 Alpha slitta ancora!
created: 1490125060
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Il rilascio di Fedora 26 Alpha &egrave; stato rimandato al <strong>28 marzo 2017</strong>.</p>
<p>Alcuni bug bloccanti hanno infatti ritardato la creazione di immagini RC (&quot;release candidates&quot;), indispensabili per l&#39;esecuzione degli ultimi test.</p>
<p>Come di consueto, il &quot;via libera&quot; &egrave; subordinato alla decisione del &quot;Go/no-go Meeting&quot; di Gioved&igrave; 23 marzo.</p>
<p>&nbsp;</p>
<p><strong>Link utili</strong>:</p>
<ul>
	<li>
		<a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">Informazioni relative ai Go/No-go Meeting</a></li>
	<li>
		<a href="https://fedoraproject.org/wiki/Fedora_26_Alpha_Release_Criteria">Criteri di rilascio per Fedora 24 Final</a></li>
	<li>
		<a href="http://qa.fedoraproject.org/blockerbugs/milestone/26/alpha/buglist">Lista aggiornata dei bug bloccanti</a></li>
	<li>
		<a href="https://fedoraproject.org/wiki/Releases/26/Schedule">Fedora 26 release schedule</a></li>
</ul>
