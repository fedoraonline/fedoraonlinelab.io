---
categories:
- staff news
layout: ultime_news
title: Fedora 27 Beta è NO-GO!!!
created: 1505494140
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Si &egrave; ieri tenuta la riunione IRC volta a decidere in merito all&#39;eventuale rilascio della milestone Beta di Fedora 27. Il log &egrave; reperibile <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2017-09-14/f27-beta-go-no-go-meeting.2017-09-14-17.00.log.html">qui</a>.<br />
	<br />
	Il responso &egrave; stato negativo, a causa della presenza di alcuni bug bloccanti.<br />
	<br />
	&nbsp;Appuntamento alla prossima settimana per un altro Go/No-Go Meeting.<br />
	&nbsp;</p>
<p><br />
	Link utili:<br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio: <a href="https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria">https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/27/beta/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/27/beta/buglist</a><br />
	- Fedora release schedule: <a href="https://fedoraproject.org/wiki/Releases/27/Schedule">https://fedoraproject.org/wiki/Releases/27/Schedule</a></p>
<p><a a="" href="https://fedoraproject.org/wiki/Releases/24/Schedule"> </a></p>
<p class="rtejustify">&nbsp;</p>
