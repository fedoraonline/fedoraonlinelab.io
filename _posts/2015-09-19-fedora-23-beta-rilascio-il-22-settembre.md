---
categories:
- staff news
layout: ultime_news
title: 'Fedora 23 Beta: rilascio il 22 settembre!'
created: 1442615011
---
<p>Dopo il meeting Go/No-Go tenutosi ieri 17 settembre, &egrave; stato deciso che... <strong>Go</strong>! Il rilascio di Fedora 23 Beta sar&agrave; per il 22 settembre prossimo.</p>
<p>&nbsp;</p>
<p>Di seguito l&#39;annuncio da parte di Jan Kurik dell&#39;esito del meeting:</p>
<p>https://lists.fedoraproject.org/pipermail/devel-announce/2015-September/001677.html</p>
<p>Da tenere d&#39;occhio i Common bugs:</p>
<p>https://fedoraproject.org/wiki/Common_F23_bugs</p>
<p>E la Release schedule:</p>
<p>https://fedoraproject.org/wiki/Releases/23/Schedule</p>
<p>&nbsp;</p>
<p>Stay tuned!</p>
