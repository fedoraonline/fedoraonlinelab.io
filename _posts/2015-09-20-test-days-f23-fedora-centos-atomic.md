---
categories:
- staff news
layout: ultime_news
title: 'Test Days F23: Fedora/CentOS Atomic'
created: 1442755502
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>Il giorno 22 settembre prossimo &egrave; previsto un Test Days congiunto con <strong>Fedora Beta 23 Atomic</strong> e l&#39;ultima release di <strong>CentOS Atomic</strong>.</p>
<p>Per chi volesse partecipare a questa giornata di test &egrave; importante ricordare che l&#39;appuntamento &egrave; sul canale #atomic della chat IRC freenode anzich&eacute; sul solito #fedora-test-day.</p>
<p>Enjoy!</p>
