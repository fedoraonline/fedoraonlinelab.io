---
categories:
- staff news
layout: ultime_news
title: 'Fedora 23 Final dichiarata GOLD: verrà rilasciata Martedì 03/11/2015!'
created: 1446226846
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Il Go/No-go Meeting di oggi, ha sancito il via libera definitivo per il rilascio finale di <strong>Fedora 23</strong>.&nbsp; La data ufficiale rimane quindi quella di <strong>Marted&igrave; 03 Novembre 2015</strong>, con una sola settimana di ritardo rispetto a quanto inizialmente previsto.</p>
<p>Qui il log completo della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-30/f23-final-go_no_go-meeting_3.2015-10-30-16.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-30/f23-final-go_no_go-meeting_3.2015-10-30-16.00.log.html</a></p>
