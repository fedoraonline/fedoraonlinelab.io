---
categories:
- staff news
layout: ultime_news
title: Fedora 21 Beta pronta per essere rilasciata martedì 4.11.
created: 1414702879
---
<p>Al meeting di stasera &egrave; appena stato deciso che <strong>Fedora 21 Beta sar&agrave; rilasciata</strong> marted&igrave; prossimo, ovvero il <strong>4 novembre</strong>.</p>
<p>Il rilascio della versione finale per ora &egrave; previsto per il 9 dicembre.</p>
<p>I dettagli del meeting sono consultabili qui:&nbsp;<a href="http://bit.ly/1tmPWzw">http://bit.ly/1tmPWzw</a></p>
