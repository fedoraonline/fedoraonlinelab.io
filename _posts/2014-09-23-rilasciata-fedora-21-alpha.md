---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 21 Alpha
created: 1411482137
---
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">E&#39; appena stata rilasciata la versione Alpha di Fedora 21, conosciuta anche come fedora.next perch&egrave; introduce un&#39;organizzazione completamente diversa delle immagini pubblicate. I tre prodotti Server, Workstation e Cloud rappresentano le immagini principali di Fedora e sono disponibili qui:</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;"><a href="http://fedoraproject.org/it/get-prerelease">http://fedoraproject.org/it/get-prerelease</a></p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Nell&#39;immagine ISO Workstation, che &egrave; una Live, &egrave; compreso Gnome, ma se si vogliono altre Spin di Fedora, immagini ARM o Docker, basta andare sulla pagina dei download alternativi:</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;"><a href="http://fedoraproject.org/it/get-spin-prerelease">http://fedoraproject.org/it/get-spin-prerelease</a></p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Le principali novit&agrave; di ogni prodotto, insieme ad altre informazioni importanti, si possono leggere (e la lettura &egrave; davvero consigliata) nell&#39;annuncio ufficiale del rilascio di Fedora 21 Alpha:</p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;"><a href="https://lists.fedoraproject.org/pipermail/announce/2014-September/003231.html">https://lists.fedoraproject.org/pipermail/announce/2014-September/003231.html</a></p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Prima di installare Fedora 21 Alpha, come sempre, &egrave; bene dare un&#39;occhiata ai bug pi&ugrave; comuni:&nbsp;<a href="http://fedoraproject.org/wiki/Common_F20_bugs" style="color: rgb(70, 122, 167); font-weight: bold; text-decoration: none;">http://fedoraproject.org/wiki/Common_F21_bugs</a></p>
<p style="margin: 1.5em 0px; color: rgb(0, 0, 0); font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; font-size: 13px;">Buon testing a tutti!</p>
