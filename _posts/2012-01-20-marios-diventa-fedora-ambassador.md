---
categories:
- staff news
layout: ultime_news
title: MarioS diventa Fedora Ambassador
created: 1327058991
---
<p>
	Dopo l&#39;investitura di mailga come Fedora Ambassador ora anche MarioS &egrave; entrato nel gruppo degli ambasciatori della nostra amata distro.</p>
<p>
	MarioS prima di diventare Ambassador aveva gi&agrave; contributo da molto tempo al Progetto Fedora come traduttore e ultimamente aveva ampliato la sfera di azione, iscrivendosi anche al gruppo dei packager e dei bugzapper. Oltre ai suoi preziosi contributi al fedoraproject e a Fol ovviamente, &egrave; presente a periodi alterni nel LUG di Trieste.</p>
<p>
	Auguriamo a Mario (e lo rinnoviamo anche a mailga) buon lavoro sperando che le sue (loro) idee possano aiutare Fedora per quanto riguarda la diffusione in Italia.</p>
