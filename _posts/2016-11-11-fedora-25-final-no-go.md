---
categories:
- staff news
layout: ultime_news
title: Fedora 25 Final è NO-GO!!!
created: 1478847927
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Si &egrave; ieri tenuta la riunione IRC volta a decidere in merito all&#39;eventuale rilascio della milestone Final di Fedora 25. Il log &egrave; reperibile <a href="https://meetbot-raw.fedoraproject.org/teams/f25-final-gono-go-meeting/f25-final-gono-go-meeting.2016-11-10-17.00.log.html">qui</a>.<br />
	<br />
	Il responso &egrave; stato negativo, a causa della presenza del seguente bug bloccante:<br />
	<br />
	<em>- <span id="summary_alias_container"><span id="short_desc_nonedit_display">no Fedora boot menu in Mac OS X dual boot install </span></span>(<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1393846">https://bugzilla.redhat.com/show_bug.cgi?id=1393846</a>)</em></p>
<p>&nbsp;</p>
<p>&nbsp;Appuntamento alla prossima settimana per un altro Go/No-Go Meeting.<br />
	&nbsp;</p>
<p><br />
	Link utili:<br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio per Fedora 25 Final: <a href="https://fedoraproject.org/wiki/Fedora_25_Final_Release_Criteria">https://fedoraproject.org/wiki/Fedora_25_Final_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/25/final/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/25/final/buglist</a><br />
	- Fedora release schedule: <a href="https://fedoraproject.org/wiki/Releases/25/Schedule">https://fedoraproject.org/wiki/Releases/25/Schedule</a></p>
<p><a a="" href="https://fedoraproject.org/wiki/Releases/24/Schedule"> </a></p>
<p class="rtejustify">&nbsp;</p>
