---
categories:
- staff news
layout: ultime_news
title: Fedora Developer Portal
created: 1446534688
---
<p><img alt="" src="http://fedoramagazine.org/wp-content/uploads/2015/11/fedoradeveloper-945x400.png" style="width: 250px; height: 106px;" /></p>
<p>Nella serata di ieri, il Fedora Project ha annunciato (<a href="http://fedoramagazine.org/announcing-fedora-developer-portal/">http://fedoramagazine.org/announcing-fedora-developer-portal/</a>) il rilascio del nuovo Fedora Developer Portal.</p>
<p>Tale sito &egrave; nato con l&#39;intento di supportare i programmatori che lavorano con Fedora, fornendo loro indicazioni a riguardo dell&#39;installazione di strumenti di sviluppo, librerie e database. Fornisce inoltre consigli ed opzioni a riguardo di distribuzione e deploy, mediante l&#39;utilizzo di strumenti quali COPR e OpenShift.</p>
<p>Per esempio, si pu&ograve; trovare come installare ed usare Vagrant con libvirt, come impostare l&#39;ambiente per l&#39;utilizzo del linguaggio Java e molto altro ancora.</p>
<p>Il link &egrave; il seguente: <a href="https://developer.fedoraproject.org/">https://developer.fedoraproject.org/</a></p>
