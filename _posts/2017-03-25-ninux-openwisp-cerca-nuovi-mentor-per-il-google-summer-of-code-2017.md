---
categories:
- staff news
layout: ultime_news
title: Ninux-OpenWISP cerca nuovi mentor per il Google Summer Of Code 2017!
created: 1490438757
---
<p><img alt="" src="http://juliux.altervista.org/images/fol/20170325_googlesummerofcode2017.jpg" style="width: 150px; height: 150px;" /></p>
<p class="rtejustify">Su gentile segnalazione di <a href="https://forum.fedoraonline.it/profile.php?id=5170">Caterpillar</a> - utente molto attivo su FOL e contributore Fedora Project - riportiamo una notizia riguardante Google Summer Of Code 2017.</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Ninux-OpenWISP sar&agrave; presente all&#39;evento in questione e ha espresso la necessit&agrave; di avere ulteriori mentor per gestire la revisione del codice e il testing.</p>
<p class="rtejustify">In linea di massima, le competenze necessarie sono Python, Django, Ansible, OpenWRT, ecc. In un futuro prossimo, la disponibilit&agrave; potrebbe anche aprire le porte verso nuove opportunit&agrave; lavorative.</p>
<p class="rtejustify">Di seguito, si riporta il link del topic completo: <a href="https://forum.fedoraonline.it/viewtopic.php?id=24961">https://forum.fedoraonline.it/viewtopic.php?id=24961</a></p>
