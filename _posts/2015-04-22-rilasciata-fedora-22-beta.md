---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 22 Beta
created: 1429690373
---
<p>Come solitamente accade, Dennis Gilmore ha annunciato il rilascio della Beta di Fedora 22:</p>
<p>https://lists.fedoraproject.org/pipermail/announce/2015-April/003261.html</p>
<p>In cui spiega anche le ultime novit&agrave;.</p>
<p>Leggete la pagina dei bug pi&ugrave; comuni:</p>
<p>https://fedoraproject.org/wiki/Common_F22_bugs</p>
<p>E se volete seguire l&#39;eventuale differimento del rilascio della versione finale:</p>
<p>https://fedoraproject.org/wiki/Releases/22/Schedule</p>
