---
categories:
- staff news
layout: ultime_news
title: 'Fedora 23 Beta: architetture AARCH64 e POWER!'
created: 1443020892
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/Fedoraarm.png" style="width: 225px; height: 224px; margin-left: 12px; margin-right: 12px; float: left;" /></p>
<p>&nbsp;</p>
<p>Dopo l&#39;annuncio del rilascio per le canoniche piattaforme 32-Bit e 64-Bit, ecco quello relativo alle architetture AARCH64 (ARM-64-Bit) e POWER (PPC64 e PPC64 Little Endian) riferito a Fedora Server ARM.<br />
	<br />
	La versione per architetture AARCH e POWER prevede le stesse migliorie presenti nelle versioni 32-Bit/64-Bit, come il miglioramento dei binari contro le vulnerabilit&agrave; di corruzione della memoria e buffer overflow, non evidente all&#39;utenza finale ma che aumentanto notevolmente la sicurezza del sistema.<br />
	&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Per leggere tutte le caratteristiche apportate in Fedora 23 Beta per architettura AARCH e POWER rimando alla lettura dell&#39;annuncio ufficiale di Peter Robinson:<br />
	<br />
	https://lists.fedoraproject.org/pipermail/announce/2015-September/003290.html<br />
	<br />
	Inoltre &egrave; possibile scaricare le immagini di Fedora ARM e reperire ulteriori informazioni su Fedora Server ARM e Desktop Computing al sito ufficiale del Fedora Project:<br />
	<br />
	https://arm.fedoraproject.org/<br />
	&nbsp;</p>
<p>Enjoy!</p>
