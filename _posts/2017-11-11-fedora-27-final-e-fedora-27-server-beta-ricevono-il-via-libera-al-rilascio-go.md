---
categories:
- staff news
layout: ultime_news
title: Fedora 27 Final e Fedora 27 Server (Beta) ricevono il via libera al rilascio
  (GO)!
created: 1510393877
---
<p><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="height:100px; width:100px" /></p>

<p>&nbsp;</p>

<p>Gioved&igrave; sera, si sono tenute le riunioni IRC volte a decidere in merito all&#39;eventuale rilascio delle seguenti milestone:</p>

<p>- Fedora 27 Final sar&agrave; rilasciata il 14/11/17: il log &egrave; consultabile <a href="https://meetbot.fedoraproject.org/fedora-meeting-1/2017-11-09/f27-final-and-server-beta-go-no-go-meeting.2017-11-09-18.02.html">qui</a>;</p>

<p>- Fedora 27 Server Beta sar&agrave; rilasciata il 14/11/17: il log &egrave; consultabile <a href="https://meetbot.fedoraproject.org/fedora-meeting-1/2017-11-09/f27-final-and-server-beta-go-no-go-meeting.2017-11-09-18.02.html">qui</a>.<br />
<br />
Il responso &egrave; stato positivo per enrrambe.<br />
<br />
&nbsp;<br />
Link utili:<br />
- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
- Criteri di rilascio (F27 Beta): <a href="https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria">https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria</a><br />
- Criteri di rilascio (F27 Final): <a href="https://fedoraproject.org/wiki/Fedora_27_Final_Release_Criteria">https://fedoraproject.org/wiki/Fedora_27_Final_Release_Criteria</a><br />
- Lista aggiornata dei bug bloccanti (F27 Final): <a href="http://qa.fedoraproject.org/blockerbugs/milestone/27/final/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/27/final/buglist</a><br />
- Fedora release schedule: <a href="https://fedoraproject.org/wiki/Releases/27/Schedule">https://fedoraproject.org/wiki/Releases/27/Schedule</a></p>

<p>&nbsp;</p>

<p>&nbsp;</p>
