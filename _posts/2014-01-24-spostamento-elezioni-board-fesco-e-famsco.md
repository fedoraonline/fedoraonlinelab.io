---
categories:
- staff news
layout: ultime_news
title: Spostamento elezioni Board, FESCo e FAmSCo!
created: 1390563414
---
<p>Era nell&#39;aria e non si poteva non affrontare il discorso.</p>
<p>Ieri, durante il Board Meeting &egrave; stato deciso di allungare il periodo delle votazioni, che avrebbero dovuto cominciare il 23 gennaio 2014. E&#39; stato deciso di affidare il processo a due persone invece che a una sola, quindi &egrave; probabile che il periodo di votazioni venga utilizzato per riaprire le nomination.</p>
<p>Seguir&agrave; il periodo delle votazioni che a questo punto sar&agrave; in febbraio, appena saranno note vi informeremo sulle date esatte. Ricordiamo che per votare &egrave; necessario far parte di almeno un gruppo che non sia il semplice CLA.</p>
