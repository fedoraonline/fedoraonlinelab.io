---
categories:
- staff news
layout: ultime_news
title: Ebook Fedora ora anche per Kindle
created: 1409561085
---
<p>In aggiunta alla pubblicazione del ebook <strong>Fedora 20 - Un sistema Desktop a portata di tutti</strong>, i cui dettagli si posso leggere nella <a href="http://www.fedoraonline.it/content/fedora-20-un-sistema-desktop-portata-di-tutti">pagina dedicata</a>, si comunica che il libro ora &egrave; disponibile anche su <a href="http://www.amazon.it/Fedora-20-sistema-Desktop-portata-ebook/dp/B00N6XR15U/ref=sr_1_2?s=digital-text">Amazon</a> per chi volesse leggerlo sul suo dispositivo Kindle.</p>
<p>Buona lettura a tutti!</p>
