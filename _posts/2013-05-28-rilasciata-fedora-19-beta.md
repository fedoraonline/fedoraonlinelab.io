---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 19 Beta
created: 1369758152
---
<p>&nbsp;</p>
<p>E&#39; stata appena rilasciata la Beta di Fedora 19, nome in codice&nbsp;<u>Schr&ouml;dinger&#39;s Cat.</u></p>
<p>La Beta contiene tutte le&nbsp;feature della versione finale con codice completato, per cui da adesso in poi si lavora prevalentemente sulle correzioni di bug e tutti sono invitati a contribuire alla fase di test. La versione finale &egrave; prevista per il 2 luglio.</p>
<p>Release Notes:&nbsp;<a href="http://fedoraproject.org/wiki/F19_Beta_release_announcement">http://fedoraproject.org/wiki/F19_Beta_release_announcement</a>&nbsp;</p>
<p>Common Bugs:&nbsp;<a href="https://fedoraproject.org/wiki/Common_F19_bugs">https://fedoraproject.org/wiki/Common_F19_bugs</a></p>
<p>Di seguito potete leggere l&#39;annuncio ufficiale della Beta:&nbsp;<a href="http://lists.fedoraproject.org/pipermail/announce/2013-May/003161.html">http://lists.fedoraproject.org/pipermail/announce/2013-May/003161.html</a></p>
