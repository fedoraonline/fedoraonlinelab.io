---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 21 Beta
created: 1415114924
---
<p>Pochi attimi fa &egrave; stata rilasciata la versione Beta di Fedora 21. Rispetto alla Alpha contiene molti aggiornamenti in pi&ugrave; e di conseguenza &egrave; molto pi&ugrave; vicina a quello che poi sar&agrave; la versione finale, prevista per il 9 dicembre.</p>
<p>Tra le novit&agrave; &egrave; da citare sicuramente la patch per shellshock, cos&igrave; come l&#39;ultimo Gnome o Wayland. Per l&#39;annuncio ufficiale vi invito a cliccare sul seguente link:</p>
<p><a href="https://lists.fedoraproject.org/pipermail/announce/2014-November/003238.html">https://lists.fedoraproject.org/pipermail/announce/2014-November/003238.html</a></p>
<p>E&#39; importante leggere i Common Bugs noti, tra cui spicca un warning per evitare di aggiornare la propria Fedora alla Beta tramite FedUp.</p>
<p><a href="https://fedoraproject.org/wiki/Common_F21_bugs">https://fedoraproject.org/wiki/Common_F21_bugs</a></p>
