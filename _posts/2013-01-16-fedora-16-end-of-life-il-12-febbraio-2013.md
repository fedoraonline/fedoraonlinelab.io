---
categories:
- staff news
layout: ultime_news
title: 'Fedora 16: end of life il 12 febbraio 2013'
created: 1358327798
---
<p>Come di consueto, con il rilascio di una nuova versione di Fedora viene dichiarato anche la data in cui &quot;morir&agrave;&quot; la versione -2, che attualmente &egrave; la 16.</p>
<p>Non solo dopo il 12 febbraio Fedora 16 non verr&agrave; pi&ugrave; mantenuta, ma fin da subito non verranno inclusi nuovi pacchetti per quella versione. Chi dovesse ancora avere Fedora 16 installato pu&ograve; aggiornarla alla versione 17 utilizzando il tool Preupgrade. Per un ulteriore aggiornamento alla 18 invece si dovr&agrave; utilizzare il nuovo FedUp.</p>
