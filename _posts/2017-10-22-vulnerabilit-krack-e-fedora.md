---
categories:
- staff news
layout: ultime_news
title: Vulnerabilità KRACK e Fedora
created: 1508658943
---
<p><img alt="" src="https://fedoramagazine.org/wp-content/uploads/2017/10/KRACK-945x400.png" style="height:200px; width:473px" /></p>

<p>Recentemente, &egrave; stata scoperta e sistemata una vulnerabilit&agrave; relativa alle reti Wi-Fi, chiamata <a href="https://arstechnica.com/information-technology/2017/10/severe-flaw-in-wpa2-protocol-leaves-wi-fi-traffic-open-to-eavesdropping/">KRACK (&quot;Key Reinstallation Attack&quot;)</a>.</p>

<p>Si raccomanda quindi di mantenere il proprio sistema aggiornato. Il pacchetto contenente le varie correzioni, &egrave; chiamato &quot;<strong>wpa_supplicant</strong>&quot;.</p>

<p>All&#39;interno dei sistemi Fedora supportati al momento, occorre la versione seguente (oppure una pi&ugrave; recente, contraddistinta da un numero identificativo maggiore):</p>

<ul>
	<li>Per <strong>Fedora 27:</strong> wpa_supplicant-2.6-11.fc27</li>
	<li>Per <strong>Fedora 26:</strong>&nbsp;wpa_supplicant-2.6-11.fc26</li>
	<li>Per <strong>Fedora 25:</strong>&nbsp;wpa_supplicant-2.6-3.fc25.1</li>
</ul>

<p>Per verificare se il sistema &egrave; affetto dal problema in oggetto quindi, &egrave; sufficiente consultare l&#39;output del seguente comando:</p>

<p><em><strong>$ rpm -q wpa_supplicant</strong></em></p>

<p>All&#39;interno del forum, &egrave; stata aperta una <a href="https://forum.fedoraonline.it/viewtopic.php?id=25294">discussione</a> relativa all&#39;argomento.</p>
