---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 18 Beta (Exploding Turkey)
created: 1354006519
---
<p>
	Come gi&agrave; annunciato qualche giorno fa, oggi &egrave; stata <strong>rilasciata la Beta Release di Fedora 18</strong>. Le novit&agrave; introdotte dalla Release attuale hanno fatto s&igrave; che le versioni testing siano arrivate con&nbsp;un notevole&nbsp;ritardo, ma da adesso in poi dovremmo essere abbastanza tranquilli sulla data di rilascio della versione stabile; infatti non dovrebbe pi&ugrave; scostarsi pi&ugrave; di tanto dal 8 gennaio 2013.</p>
<p>
	Le principali novit&agrave; introdotte sono le seguenti:</p>
<ul>
	<li>
		Nuova interfaccia dell&#39;installer Anaconda</li>
	<li>
		Secure Boot</li>
	<li>
		Fedup (che sostituisce preuprade)</li>
	<li>
		Gnome 3.6</li>
	<li>
		Kde Plasma Workspace 4.9</li>
	<li>
		Python 3.3</li>
	<li>
		RPM 4.1</li>
	<li>
		Samba 4</li>
	<li>
		/tmp su tmpfs</li>
	<li>
		XFCE 4.1</li>
	<li>
		Mate-Desktop</li>
</ul>
<p>
Tutte le features di Fedora 18, alcuni disponibili solo nella versione stabile, sono elencati qui:
<a href="http://fedoraproject.org/wiki/Releases/18/FeatureList">http://fedoraproject.org/wiki/Releases/18/FeatureList</a>

Per chi volesse provare la versione Beta, pu&ograve; scaricare le ISO disponibili da qui:
<a href="http://fedoraproject.org/get-prerelease">http://fedoraproject.org/get-prerelease</a>

<strong>ATTENZIONE:</strong> Leggere attentamente i Common Bugs di &quot;Exploding Turkey&quot;, come &egrave; stata chiamata Fedora 18 Beta: <a href="https://fedoraproject.org/wiki/Common_F18_bugs">https://fedoraproject.org/wiki/Common_F18_bugs</a>

Per ogni altra informazione rimandiamo alle <a href="http://fedorapeople.org/groups/docs/release-notes/it-IT/">Note di Rilascio.</a></p>
