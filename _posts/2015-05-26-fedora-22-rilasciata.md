---
categories:
- staff news
layout: ultime_news
title: Fedora 22 rilasciata!
created: 1432654782
---
<p>A poco pi&ugrave; di 6 mesi da Fedora 21, oggi &egrave; stata rilasciata Fedora 22. Le novit&agrave; sono molte, non solo dal punto di vista degli aggiornamenti, ma anche da un punto di vista di proggeto, dal momento che &egrave; arrivata la differenziazione tra spin e labs, nuovi siti web e nuove edizioni.</p>
<p>Vi segnalo&nbsp;<a href="http://fedoramagazine.org/fedora-22-released/">l&#39;annuncio ufficiale su fedoramagazine</a>&nbsp;dove potete rendervi conto del lavoro svolto.</p>
<p><img alt="" src="http://fedoramagazine.org/wp-content/uploads/2015/05/fedora22-945x400.png" style="width: 600px; height: 300px;" /></p>
<p>Ultimo ma non ultimo, abbiamo anche diversi nuovi siti web come <a href="http://arm.fedoraproject.org">arm</a>, <a href="http://spins.fedoraproject.org">spins</a> e <a href="https://labs.fedoraproject.org">labs</a>. Buon divertimento!</p>
