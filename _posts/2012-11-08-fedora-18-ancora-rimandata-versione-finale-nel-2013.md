---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18 ancora rimandata: versione finale nel 2013!'
created: 1352359572
---
<p>
	Dopo molti rimandi di settimana in settimana e altrettanti discussioni sul metodo di lavoro, sulle procedure di rilascio e su un eventuale rolling release, ieri &egrave; stato deciso che la Beta di Fedora 18 <u>slitter&agrave; di altre due settimane</u>, nella speranza di riuscire a mettere insieme correttamente il nuovo installer Anaconda, il nuovo upgrade tool e il Secure Boot.</p>
<p>
	La nuova data di rilascio di Fedora 18 Beta &egrave; quindi&nbsp;<strong>marted&igrave;&nbsp;27 novembre 2012</strong>.</p>
<p>
	Di conseguenza la <u>versione finale </u>slitter&agrave; all&#39;anno prossimo, ovvero al <u>8 gennaio 2013</u>.</p>
<p>
	Tutta la release schedule &egrave; disponibile al seguente indirizzo: <a href="https://fedoraproject.org/wiki/Releases/18/Schedule">https://fedoraproject.org/wiki/Releases/18/Schedule</a></p>
