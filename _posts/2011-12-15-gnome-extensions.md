---
categories:
- staff news
layout: ultime_news
title: Gnome Extensions
created: 1323985262
---
<p>
	Gnome-Shell pu&ograve; essere personalizzata mediante le estensioni.<br />
	<a href="http://extensions.gnome.org/">Gnome Extensions</a> &egrave; il sito ufficiale di GNOME per installare estensioni in Gnome-Shell.</p>
<div>
	<a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Gnomelogo.svg/200px-Gnomelogo.svg.png?uselang=it"><img border="0" src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Gnomelogo.svg/200px-Gnomelogo.svg.png?uselang=it" /></a></div>
<p>
	<br />
	Gnome Extensions permette di scegliere tra alcune estensioni disponibili ed installarle facilmente in Gnome-Shell,inoltre le estensioni installate sono sicure perch&egrave; ogni estensione caricata viene sottoposta ad un controllo.</p>
<p>
	Gnome Extensions supporta la versione Gnome 3.2, motivo per cui &egrave; necessario avere almeno Fedora 16. La filosofia &egrave; quella delle estensioni di Mozilla, leggero, funzionale e facile da usare.</p>
