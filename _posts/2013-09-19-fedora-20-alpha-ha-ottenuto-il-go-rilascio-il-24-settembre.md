---
categories:
- staff news
layout: ultime_news
title: Fedora 20 Alpha ha ottenuto il GO - rilascio il 24 settembre!
created: 1379618848
---
<p>Dopo una settimana di ulteriore sviluppo in cui sono stati prodotti ben due Release Candidate ulteriori, la RC4 ha ottenuto poco tempo fa il famoso &quot;GO&quot;, ovvero la decisione di poter essere rilasciata come Alpha release.</p>
<p>Il rilascio quindi avverr&agrave; marted&igrave; 24 settembre, solito orario delle 10 EST (ore 16 italiane), buon testing a tutti!</p>
<p>Log del meeting: http://bit.ly/16iEjuO</p>
