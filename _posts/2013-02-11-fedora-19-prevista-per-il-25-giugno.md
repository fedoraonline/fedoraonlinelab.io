---
categories:
- staff news
layout: ultime_news
title: Fedora 19 prevista per il 25 giugno
created: 1360614509
---
<p>E&#39; uscita la, spero definitiva, Release Schedule per la prossima release di Fedora:</p>
<p><a href="https://fedoraproject.org/wiki/Releases/19/Schedule">https://fedoraproject.org/wiki/Releases/19/Schedule</a></p>
<p>Da quello che si legge la Alpha uscir&agrave; il 16 aprile, la beta il 21 maggio e se tutto va secondo i piani avremo una nuova Fedora giusto giusto in tempo per le ferie estive, il 25 giugno.</p>
