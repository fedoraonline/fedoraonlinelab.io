---
categories: []
layout: page
title: Informativa Cookie
created: 1434615506
---
<header class="entry-header" style="color: rgb(55, 55, 55); font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 24.375px;">
	<h1 class="entry-title" style="border: 0px; font-family: inherit; font-size: 26px; font-style: inherit; margin: 0px; outline: 0px; padding: 15px 76px 0.3em 0px; vertical-align: baseline; clear: both; color: rgb(34, 34, 34); line-height: 1.5em;">
		Informativa Cookies</h1>
</header>
<div class="entry-content" style="border: 0px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; margin: 0px; outline: 0px; padding: 1.625em 0px 0px; vertical-align: baseline; color: rgb(55, 55, 55); line-height: 24.375px;">
	<p style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">ll sito fedoraonline.it &egrave; settato in modo da utilizzare solo cookie tecnici (vedi definizione di &ldquo;cookie&rdquo; qui di seguito).</p>
	<h6 style="border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both;">
		COOKIE USATI:</h6>
	<ul style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em 2.5em; outline: 0px; padding: 0px; vertical-align: baseline; list-style: square;">
		<li style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;">
			Dato che questo sito &egrave; stato realizzato utilizzando le piattaforme FluxBB, Drupal, MediaWiki e Bibloplanet, utilizza cookie tecnici di queste piattaforme per gestire il login.</li>
		<li style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;">
			In diversi punti viene settato un cookie &#39;has_js&#39; che pu&ograve; assumere i valori &#39;0&#39; o &#39;1&#39; e serve per ottimizzare l&#39;esperienza utente sulla base che il suo browser supporti o meno i javascript.</li>
	</ul>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">ATTENZIONE: Nel sito fedoraonline.it vi sono pagine ed articoli di collegamento (link) con altri siti web. Detti siti potrebbero utilizzare cookie; &egrave; responsabilit&agrave; di questi siti assicurare la loro conformit&agrave; con la normativa vigente circa i cookie.</p>
	<h4 class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both;">
		<u><b>DEFINIZIONE DI &ldquo;COOKIE&rdquo;</b></u></h4>
	<h5 class="western" style="border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both;">
		<b>Cosa sono i cookie</b></h5>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">I cookie sono file di piccole dimensioni che vengono memorizzati all&rsquo;interno del proprio computer (o tablet, o smartphone) quando si visita un sito web. I siti web possono utilizzare i cookie per personalizzare l&rsquo;esplorazione degli utenti e raccogliere informazioni sull&rsquo;utilizzo del sito.</p>
	<h5 style="border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both;">
		<b>Tipologie di cookie</b></h5>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">In base alle caratteristiche e all&rsquo;utilizzo dei cookie si possono distinguere diverse categorie:</p>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;"><u>Cookie tecnici:&nbsp;</u>Sono i cookie che servono a effettuare la navigazione o a fornire un servizio richiesto dall&rsquo;utente. Non vengono utilizzati per scopi ulteriori e sono normalmente installati direttamente dal titolare del sito web. Senza il ricorso a tali cookie, alcune operazioni non potrebbero essere compiute o sarebbero pi&ugrave; complesse e/o meno sicure.</p>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">Assimilati ai cookie tecnici sono quelli utilizzati per raccogliere e analizzare il traffico e l&rsquo;utilizzo del sito in modo anonimo. La disattivazione di tali cookie pu&ograve; essere eseguita senza alcuna perdita di funzionalit&agrave;.</p>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;"><u>Cookie di profilazione:</u>&nbsp;sono i cookie utilizzati per tracciare la navigazione dell&rsquo;utente in rete e creare profili sui suoi gusti, abitudini, scelte, ecc. Con questi cookie possono essere trasmessi al terminale dell&rsquo;utente messaggi pubblicitari in linea con le preferenze gi&agrave; manifestate dallo stesso utente nella navigazione online. Questo sito non usa cookie di profilazione.</p>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;"><u>Cookie di terze parti:</u>&nbsp;visitando un sito web si possono ricevere cookie sia dal sito visitato (&ldquo;proprietari&rdquo;), sia da siti gestiti da altre organizzazioni (&ldquo;terze parti&rdquo;). Un esempio tipico &egrave; rappresentato dalla presenza dei &ldquo;social plugin&rdquo; per Facebook, Twitter e YouTube. Si tratta di parti della pagina visitata generate direttamente dai suddetti siti ed integrati nella pagina del sito ospitante. L&rsquo;utilizzo pi&ugrave; comune dei social plugin &egrave; finalizzato alla condivisione dei contenuti sui social network.</p>
	<h5 class="western" style="border: 0px; font-family: inherit; font-size: 15px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both;">
		<b>Gestione dei cookie</b></h5>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">L&rsquo;utente pu&ograve; decidere se accettare o meno i cookie utilizzando le impostazioni del proprio browser.</p>
	<p class="western" style="border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px 0px 1.625em; outline: 0px; padding: 0px; vertical-align: baseline;">Attenzione: la disabilitazione totale o parziale dei cookie tecnici pu&ograve; compromettere l&rsquo;utilizzo delle funzionalit&agrave; del sito riservate agli utenti registrati. Al contrario, la fruibilit&agrave; dei contenuti pubblici &egrave; possibile anche disabilitando completamente i cookie.</p>
</div>
<p>&nbsp;</p>
