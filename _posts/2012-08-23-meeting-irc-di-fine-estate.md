---
categories:
- staff news
layout: ultime_news
title: Meeting IRC di fine estate
created: 1345756868
---
<p>
	Il prossimo meeting IRC, come riportato anche nella pagina dedicata su <a href="http://doc.fedoraonline.it/Meeting_IRC">http://doc.fedoraonline.it/Meeting_IRC</a> &egrave; previsto per</p>
<p class="rtecenter">
	<strong>gioved&igrave; 30 agosto 2012, ore 22:00</strong></p>
<p>
	Agenda prevista:</p>
<ul>
	<li>
		Boutique: aggiornamento</li>
	<li>
		Folio: temi e redattori</li>
	<li>
		Votazione date e ricorrenze Meeting IRC</li>
	<li>
		Eventi fine anno 2012</li>
</ul>
<p>
	Come sempre l&#39;incontro si svolger&agrave; sul canale #fedora-it di freenode e sar&agrave; logged. La durata prevista &egrave; di un&#39;ora.<br />
	Ulteriori dettagli si trovano sulla pagina del wiki.</p>
