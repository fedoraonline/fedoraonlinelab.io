---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 19 Alpha
created: 1366728149
---
<p>E&#39; stata appena rilasciata la prima versione di Fedora 19, nome in codice <u>Schr&ouml;dinger&#39;s Cat.</u></p>
<p>La Alpha contiene tutte le&nbsp;feature della versione finale, ovviamente ancora in una fase test, in modo che tutti possono aiutare a sviluppare il codice. Una volta completo il codice sar&agrave; giunta l&#39;ora della Beta, prevista per il 28 maggio. Le novit&agrave; pi&ugrave; importanti sono molteplici, ed &egrave; importante contribuire al loro sviluppo facendo qualche test.</p>
<p>Release Notes: <a href="http://fedoraproject.org/wiki/Fedora_19_Alpha_release_notes">http://fedoraproject.org/wiki/Fedora_19_Alpha_release_notes</a>
Common Bugs: <a href="https://fedoraproject.org/wiki/Common_F19_bugs">https://fedoraproject.org/wiki/Common_F19_bugs</a></p>
<p>Mentre di seguito potete leggere l&#39;annuncio ufficiale della Alpha:
<a href="http://lists.fedoraproject.org/pipermail/test-announce/2013-April/000662.html">http://lists.fedoraproject.org/pipermail/test-announce/2013-April/000662.html</a></p>
