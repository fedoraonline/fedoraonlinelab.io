---
categories:
- staff news
layout: ultime_news
title: 'Kevin Fenzi: come verificare le immagini Fedora'
created: 1456576501
---
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Il recente avvenimento riguardante la compromissione del portale di <a href="http://blog.linuxmint.com/?p=2994">Linux Mint</a>, ha suscitato molto clamore all&#39;interno del mondo Open Source.</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Kevin &quot;nirik&quot; Fenzi, membro del team Infrastructure, ha scritto un interessante <a href="https://www.scrye.com/wordpress/nirik/2016/02/23/a-fedora-distribution-download-primer/">articolo</a> a riguardo di come controllare l&#39;integrit&agrave; delle immagini di Fedora scaricate. Riportiamo di seguito la traduzione italiana del post.</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">&nbsp;</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Con la recente notizia della compromissione delle ISO della distribuzione Linux Mint, ho pensato di <span style="background: transparent">impiegare</span> alcuni minuti per spiegare come Fedora gestisce il download delle immagini e cosa, l&#39;utente, pu&ograve; fare per assicurarsi di aver reperito l&#39;ufficiale e corretta versione di Fedora.</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Per prima cosa, possiamo esaminare, passaggio dopo passaggio, cosa succede quando si apre il browser per raggiungere getfedora.org, il nostro portale per il download delle immagini per l&#39;installazione:</p>
<ul>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">L&#39;utente scrive &ldquo;getfedora.org&rdquo; nel browser</p>
	</li>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Per prima cosa, il sistema operativo dell&#39;utente chiede ai server DNS l&#39;indirizzo IP di getfedora.org. Se si sta utilizzando dnssec, si ricever&agrave; una risposta firmata e cifrata<span style="background: transparent">. In caso contrario, si ricever&agrave; una </span><span style="background: transparent">normale</span><span style="background: transparent"> risposta </span><span style="background: transparent">proveniente</span><span style="background: transparent"> d</span><span style="background: transparent">a</span><span style="background: transparent">l DNS.</span></p>
	</li>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Il browser quindi, potrebbe tentare di connettersi a getfedora.org utilizzando http. Abbiamo impostato getfedora.org per redirigere tutte le richieste ad https.</p>
	</li>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Con la prima connessione https a getfedora.org, mandiamo un header HSTS. Quest&#39;ultimo, informa il browser dell&#39;utente affinch&eacute; esso utilizzi sempre https per usufruire delle pagine sito (se il browser supporta tale meccanismo).</p>
	</li>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Una volta connesso al sito, l&#39;utente pu&ograve; utilizzare l&#39;apposito link per scaricare l&#39;immagine che preferisce. Successivamente, se Javascript non &egrave; stato completamente disabilitato, appare una schermata che descrive come verificare il download: <a href="https://getfedora.org/it/verify">https://getfedora.org/it/verify</a>.</p>
	</li>
	<li>
		<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Al termine del download, per assicurarsi di essere in possesso di un&#39;immagine valida ed ufficiale, sono necessari due passaggi. Per prima cosa, occorre controllare la firma gpg del file di checksum. I file di checksum ufficiali di Fedora, sono sempre firmati. La chiave gpg relativa allo specifico rilascio, pu&ograve; essere ottenuta da getfedora.org, da praticamente tutti i keyserver, o dal pacchetto fedora-repos se si &egrave; gi&agrave; in possesso di un sistema Fedora installato. Inoltre, se l&#39;utente importa la chiave ed opera un refresh (gpg2 &ndash;-refresh-keys), egli pu&ograve; vedere i dettagli relativi e, in base a questi, decidere se la chiave &egrave; affidabile. Se tutto appare corretto, &egrave; possibile procedere con l&#39;utilizzo di sha256sum per controllare il checksum dell&#39;immagine. CONVIENE ESEGUIRE SEMPRE QUESTI CONTROLLI APPENA DELINEATI.</p>
	</li>
</ul>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">&nbsp;</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Concludendo&hellip; Abbiamo dnssec, hsts e file di checksum firmati. Tutto questo, ci avrebbe aiutato nel caso in cui avessimo sofferto un attacco similare a quanto subito dai ragazzi di Linux Mint? In tale intrusione, le loro macchine per il download sono state compromesse e gli <span style="background: transparent">ignoti</span> hanno rimpiazzato i checksum e i link per lo scaricamento con loro versioni. Se questo fosse capitato a Fedora, l&#39;unico passaggio della lista sopra citata, che avrebbe protetto gli utenti, sarebbe stato il controllo delle firme gpg. Purtroppo, tale precauzione non viene sempre presa, perch&eacute; appare difficile, frustrante e deve essere eseguita manualmente.</p>
<p class="rtejustify" style="margin-bottom: 0cm; line-height: 100%;">Con Fedora 24, le edizioni Workstation e Server si attiveranno per preferire l&#39;applicazione per la creazione di supporti Usb, in luogo dei download diretti delle immagini. Dobbiamo assicurarci che essa risulti il pi&ugrave; affidabile possibile, ma probabilmente saranno comunque necessari alcuni passaggi manuali per il<span style="background: transparent"> controllo dell&#39;applicazione scaricata </span><span style="background: transparent">(se essa non viene reperita come normale pacchetto all&#39;interno di un sistema Fedora gi&agrave; installato)</span>. Nella sua implementazione corrente, il programma reperisce gi&agrave; i file di checksum dal master mirror di Fedora mediante https e l&#39;utente pu&ograve; quindi controllare l&#39;integrit&agrave; del download. Si pu&ograve; comunque fare di pi&ugrave;<span style="background: transparent">.</span></p>
