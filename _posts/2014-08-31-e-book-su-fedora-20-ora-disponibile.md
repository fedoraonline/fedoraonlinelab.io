---
categories:
- staff news
layout: ultime_news
title: E-book su Fedora 20 ora disponibile
created: 1409489453
---
<p style="margin: 0px; border: 0px; font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; padding: 7px 0px; color: rgb(51, 51, 51); font-size: 13px; background-color: rgb(252, 253, 254);">Ciao a tutti!<br />
	Da oggi &egrave; disponibile l&#39;e-book&nbsp;<strong>&quot;Fedora 20 - Un sistema Desktop a portata di tutti&quot;</strong>, un libro che descrive in modo chiaro e semplice tutti i passaggi per installare, configurare e utilizzare Fedora 20. Per adesso il libro &egrave; scaricabile solo in formato epub, a giorni sar&agrave; disponibile anche su Amazon per chi avesse necessit&agrave; del formato kindle.<br />
	Per sapere di pi&ugrave; su come scaricarlo e sul libro stesso vi invito di visitare&nbsp;<a href="http://www.fedoraonline.it/content/fedora-20-un-sistema-desktop-portata-di-tutti" style="text-decoration: none; color: rgb(70, 122, 167); font-weight: bold;">la pagina dedicata</a>&nbsp;e se avete piacere, di&nbsp;<a href="http://bitbiz.org/robyduck/fedora-20-un-sistema-desktop-a-portata-di-tutti/" style="text-decoration: none; color: rgb(70, 122, 167); font-weight: bold;">condividere la notizia sui social network</a>.</p>
<p style="margin: 0px; border: 0px; font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; padding: 7px 0px; color: rgb(51, 51, 51); font-size: 13px; background-color: rgb(252, 253, 254);">Come alcuni di voi ricorderanno, avevo cercato una soluzione per alimentare in qualche modo la comunit&agrave; fedoraonline.it, ma non me la sono sentita di mettere dei pop-up, banner o altro su un sito come il nostro. Ecco da dove &egrave; nata l&#39;idea di rifare una seconda edizione del libro, allora cartaceo, su Fedora e di utilizzare tutto il ricavato per tenere in piedi i server su cui si appoggia FOL.</p>
<p style="margin: 0px; border: 0px; font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; padding: 7px 0px; color: rgb(51, 51, 51); font-size: 13px; background-color: rgb(252, 253, 254);">Il pagamento, ad eccezione per la versione kindle per la quale valgono i metodi previsti da Amazon, si effettua solo ed esclusivamente tramite Paypal. Se ci fosse bisogno e qualcuno volesse utilizzare un metodo pi&ugrave; manuale, tramite postepay per esempio, siete pregati di mettervi in contatto con me via mail.</p>
<p style="margin: 0px; border: 0px; font-family: 'DejaVu Sans-Serif', Verdana, sans-serif; padding: 7px 0px; color: rgb(51, 51, 51); font-size: 13px; background-color: rgb(252, 253, 254);">Buona lettura!</p>
