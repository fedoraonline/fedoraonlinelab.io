---
categories:
- staff news
layout: ultime_news
title: '23/11/2015: Fedora 23 Release Party Milano (19:00 - 21:00)'
created: 1448301130
---
<p><img alt="" src="https://fedoraproject.org/w/uploads/e/ee/FedoraEvents_Header_EventN1.png" style="width: 468px; height: 60px;" /></p>
<p>&nbsp;</p>
<p>Nella serata di oggi, dalle <strong>ore 19:00 alle ore 21:00</strong>, si terr&agrave; un Release Party tutto italiano, dedicato a Fedora.</p>
<p>L&#39;indirizzo &egrave; il seguente:&nbsp; <b>Via Copernico 38, 20125 - Milano</b>.</p>
<p>Per i dettagli (mappa, argomenti delle presentazioni e altro), &egrave; possibile consultare la seguente pagina ufficiale: <a href="https://fedoraproject.org/wiki/Release_Party_F23_Milan">https://fedoraproject.org/wiki/Release_Party_F23_Milan</a></p>
