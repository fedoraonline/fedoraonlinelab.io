---
categories:
- staff news
layout: ultime_news
title: Fedora 24 Final slitta di una settimana
created: 1465494690
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Si &egrave; oggi tenuta la riunione IRC volta a decidere in merito all&#39;eventuale rilascio della milestone Final di Fedora 24. Il log &egrave; reperibile <a href="https://meetbot.fedoraproject.org/fedora-meeting-1/2016-06-09/f24-final-go_no_go-meeting.2016-06-09-17.11.log.html">qui</a>.<br />
	<br />
	Il responso &egrave; stato negativo, a causa della mancanza di una RC e della presenza dei seguenti bug bloccanti. I primi due sono gi&agrave; stati gi&agrave; risolti, ma gli aggiornamenti interessati hanno bisogno di alcuni giorni per assicurare un sufficiente livello qualitativo ed evitare lo slittamento:<br />
	<br />
	<em>- Server-dvd doesn&#39;t boot from flash drive created by dd (<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1331317">https://bugzilla.redhat.com/show_bug.cgi?id=1331317</a>)</em></p>
<p><em>- initramfs are no longer hostonly by default (<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1331834">https://bugzilla.redhat.com/show_bug.cgi?id=1331834</a>)</em></p>
<p><em><em>- chainloading bootmgr.efi on UEFI results in error: out of memory (<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1320273">https://bugzilla.redhat.com/show_bug.cgi?id=1320273</a>)</em></em></p>
<p><br />
	&nbsp;</p>
<p>&nbsp;Appuntamento a Gioved&igrave; 16 Giugno 2016 (ore 19:00 italiane) per un altro Go/No-Go Meeting.<br />
	&nbsp;La nuova data per il rilascio &egrave; invece prevista per il <u><strong>21 Giugno 2016</strong></u> (ovvviamente subordinata alla decisione finale dell&#39;incontro del 16).</p>
<p><br />
	Link utili:<br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio per Fedora 24 Final: <a href="https://fedoraproject.org/wiki/Fedora_24_Final_Release_Criteria">https://fedoraproject.org/wiki/Fedora_24_Final_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/24/final/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/24/final/buglist</a><br />
	- Fedora 24 release schedule: <a href="https://fedoraproject.org/wiki/Releases/24/Schedule">https://fedoraproject.org/wiki/Releases/24/Schedule</a></p>
<p><a a="" href="https://fedoraproject.org/wiki/Releases/24/Schedule"> </a></p>
<p class="rtejustify">&nbsp;</p>
