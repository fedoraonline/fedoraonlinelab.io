---
categories:
- staff news
layout: ultime_news
title: Aggiornamento download link fedoraproject
created: 1329816005
---
<p>
	Per gli ultimi due anni e mezzo il link <a href="http://download.fedora.redhat.com">http://download.fedora.redhat.com</a> &egrave; stato un DNS alias per <a href="http://dl.fedoraproject.org">http://dl.fedoraproject.org</a>.</p>
<p>
	Oltre alle difficolt&agrave; del protocollo https nella gestione dell&#39;alias, questo spesso ha portato gli utenti a contattare direttamente RedHat per richieste di aiuto. Per questi motivi il fedoraproject ha annunciato che l&#39;alias &egrave; stato rimosso dai DNS.</p>
Si prega quindi di aggiornare tutti i link negli script, nelle documentazioni, nei link o in altre risorse, con il nuovo indirizzo e fare cos&igrave; riferimento al mirror principale, gestito dall&#39;infrastruttura del Progetto Fedora.<br />
Inoltre, si potr&agrave; usare <a href="http://download.fedoraproject.org">http://download.fedoraproject.org</a> per utilizzare il gestore dei mirror che fornisce il mirror pi&ugrave; vicino utilizzando <span>GeoIP,</span> <span>netblock o ASN.</span>
<p>
	<span>Fonte ML: </span><span><a>announce@lists.fedoraproject.org</a></span><span> </span></p>
