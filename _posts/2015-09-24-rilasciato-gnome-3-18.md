---
categories:
- staff news
layout: ultime_news
title: Rilasciato Gnome 3.18!
created: 1443074530
---
<p class="rtecenter"><img alt="" src="http://fedoramagazine.org/wp-content/uploads/2015/09/gnome3-18-945x400.png" style="width: 354px; height: 150px;" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Nella giornata di oggi, il team di Gnome ha comunicato il rilascio della versione 3.18 (nome in codice &quot;Gothenburg&quot;) dell&#39;ambiente desktop predefinito di Fedora Workstation</p>
<p>Le principali novit&agrave; riguardano:</p>
<ul>
	<li>
		Integrazione di Google Drive in File (Nautilus).</li>
	<li>
		Aggiornamenti firmware/BIOS tramite Software.</li>
	<li>
		Gestione luminosit&agrave; automatica.</li>
	<li>
		Movimenti del Touchpad.</li>
	<li>
		Molte nuove applicazioni: tra cui Calendar e Characters.</li>
	<li>
		Miglioramenti significativi per Files (Nautilus), Boxes e Polari.</li>
	<li>
		Svariati bug risolti.</li>
</ul>
<p>&nbsp;</p>
<p>Maggiori dettagli sono disponibili nella <a href="https://www.gnome.org/news/2015/09/gnome-3-18-released-brings-big-improvements/">pagina ufficiale</a> dell&#39;annuncio.</p>
<p>Il modo migliore per utilizzare e sottoporre a test Gnome 3.18, &egrave; quello di procedere all&#39;installazione e all&#39;aggiornamento del rilascio <a href="https://getfedora.org/en/workstation/prerelease/">Beta</a> di Fedora 23 Workstation.</p>
<p>&nbsp;</p>
