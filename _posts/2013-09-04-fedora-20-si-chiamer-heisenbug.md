---
categories:
- staff news
layout: ultime_news
title: Fedora 20 si chiamerà Heisenbug!
created: 1378288103
---
<p>Si &egrave; conclusa la votazione per il nome di Fedora 20, come annunciato ufficialmente ieri:</p>
<p><a href="https://lists.fedoraproject.org/pipermail/announce/2013-September/003181.html">https://lists.fedoraproject.org/pipermail/announce/2013-September/003181.html</a></p>
<p>Heisenbug, il nome in codice di Fedora 20, sar&agrave; rilasciata il 26 novembre, ma per chi vuole provare una prima versione &egrave; quasi pronta Fedora 20 Alpha, che verr&agrave; rilasciata il 17 settembre.</p>
