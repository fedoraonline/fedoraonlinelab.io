---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 18 Alpha
created: 1347977016
---
<p>
	Dopo 3 settimane di attesa oggi &egrave; stata rilasciata la Alpha di Fedora 18 - nome in codice &#39;Spherical Cow&#39;. Ci sono parecchie novit&agrave; in questo nuovo rilascio, molte ancora sotto sviluppo; per dare un&#39;occhiata ci si pu&ograve; collegare qui:</p>
<p>
	<a href="http://fedoraproject.org/get-prerelease">http://fedoraproject.org/get-prerelease</a></p>
<p>
	Come sempre la versione Alpha &egrave; ancora molto acerba, e questo vale soprattutto per la 18, ma &egrave; anche motivo per cui ora come non mai il team di sviluppo chiede la collaborazione della comunit&agrave; per avere dei feedback e per risolvere quanto prima i bug noti.</p>
<p>
	<span style="color:#ff0000;"><strong>ATTENZIONE: leggere attentamente le Note di Rilascio e i Bug noti!</strong></span></p>
<p>
	<a href="http://fedoraproject.org/wiki/Fedora_18_Alpha_release_notes">http://fedoraproject.org/wiki/Fedora_18_Alpha_release_notes</a></p>
<p>
	<a href="http://fedoraproject.org/wiki/Common_F18_bugs">http://fedoraproject.org/wiki/Common_F18_bugs</a></p>
<p>
	Il bug pi&ugrave; importante riguarda Anaconda, ed &egrave; anche responsabile per il ritardo accumulato fino ad ora. Infatti:</p>
<ul>
	<li>
		Ananconda formatter&agrave; l&#39;intero disco anche se si seleziona manualmente una partizione</li>
	<li>
		Non &egrave; possibile fare l&#39;upgrade da Fedora 17</li>
	<li>
		Anaconda va in crash senza alcun avviso se il disco su cui si installa Fedora 18 Alpha &egrave; troppo piccolo</li>
	<li>
		Idem se si utilizza il partizionamento automatico; in caso di disco troppo piccolo va in crash</li>
	<li>
		Dopo l&#39;installazione dalla live il reboot fallisce</li>
</ul>
<p>
	Nonostante tutto la Alpha promette bene e ha a bordo moltissime novit&agrave;, Anaconda fa parte proprio di queste novit&agrave;.</p>
<p>
	Buon test a tutti!</p>
