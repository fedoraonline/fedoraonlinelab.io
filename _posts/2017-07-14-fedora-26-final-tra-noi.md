---
categories:
- staff news
layout: ultime_news
title: Fedora 26 Final è tra noi!
created: 1500055932
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2017/07/fedora26-945x400.jpg" style="width: 400px; height: 169px;" /></p>
<p>Nel corso di questa settimana<span style="display: none;"> </span>, &egrave; stata ufficilamente rilasciata la versione 26 di Fedora.</p>
<p>I link per il download, sono i seguenti:<br />
	- <a href="https://getfedora.org/workstation/">https://getfedora.org/workstation/</a><br />
	- <a href="https://getfedora.org/server/">https://getfedora.org/server/</a><br />
	- <a href="https://getfedora.org/cloud/">https://getfedora.org/cloud/</a><br />
	- <a href="https://spins.fedoraproject.org/">https://spins.fedoraproject.org/</a><br />
	- <a href="https://labs.fedoraproject.org/">https://labs.fedoraproject.org/</a><br />
	- <a href="https://arm.fedoraproject.org/">https://arm.fedoraproject.org/</a><br />
	<br />
	La wiki di Fedora Online contiene alcune utili guide per procedere all&#39;installazione o all&#39;avanzamento di versione:<br />
	- Nuova installazione (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>)<br />
	- Upgrade da rilasci precedenti (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>)<br />
	<br />
	Per la lista delle novit&agrave;, rimando i lettori al recente articolo, pubblicato da Matthew Miller per <a href="https://fedoramagazine.org/fedora-26-is-here/">Fedora Magazine</a>.<br />
	<br />
	<strong>Bug comuni</strong><br />
	La wiki del Fedora Project, contiene, come di consueto, una pagina relativa ai bug comuni. Il link &egrave; il seguente: <a href="https://fedoraproject.org/wiki/Common_F26_bugs">https://fedoraproject.org/wiki/Common_F26_bugs</a><br />
	<br />
	<br />
	&nbsp;</p>
<div id="cke_pastebin" style="position: absolute; top: 193px; width: 1px; height: 1px; overflow: hidden; left: -1000px;">
	&nbsp;</div>
