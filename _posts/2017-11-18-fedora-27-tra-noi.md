---
categories:
- staff news
layout: ultime_news
title: Fedora 27 è tra noi!!!
created: 1511002065
---
<p><em>Nel corso della settimana che si sta avviando verso la conclusione, &egrave; stato reso disponibile il rilascio finale di Fedora 27.<br />
Di seguito si propone un adattamento italiano di un <a href="https://fedoramagazine.org/announcing-fedora-27/">recente articolo</a> di <strong>Matthew Miller</strong>, esaminando alcune delle principali novit&agrave;, che accompagnano questa nuova versione.</em></p>

<p><img alt="" src="https://fedoramagazine.org/wp-content/uploads/2017/10/fedora27.png-945x400.jpg" style="height:200px; width:473px" /></p>

<p>&nbsp;</p>

<p><strong>INTRODUZIONE</strong></p>

<p>Il Fedora Project &egrave; fiero di annunciare il rilascio al pubblico delle edizioni Fedora 27 Workstation e Fedora 27 Atomic. Questa nuova versione incorpora diversi miglioramenti provenienti dalla comunit&agrave; Fedora e dagli sviluppatori dei vari software inclusi.</p>

<p>&Egrave; possibile scaricare <a href="https://getfedora.org/en/workstation/download/">Fedora 27 Workstation</a> e <a href="https://getfedora.org/en/atomic/download/">Fedora 27 Atomic Host</a> da <a href="http://getfedora.org/">getfedora.org</a>. In alternativa - per utenti che gi&agrave; utilizzano Fedora - &egrave; possibile eseguire l&#39;<a href="https://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">avanzamento di versione</a>. &Egrave; inoltre possibile scaricare <a href="https://getfedora.org/server/prerelease">Fedora 27 Beta Modular Server</a>.</p>

<p><br />
<strong>FEDORA WORKSTATION</strong></p>

<p>L&#39;edizione Workstation di Fedora 27 contiene GNOME 3.26. In questo nuovo rilascio, le interfacce per la configurazione delle impostazioni sono state aggiornate, con particolare riguardo a quelle relative a schermi e reti. La ricerca mostra ora pi&ugrave; risultati, includendo le azioni di sistema.</p>

<p>GNOME 3.26 include il supporto per le emoji colorate, la condivisione di cartelle in Boxes e numerosi miglioramenti riguardanti l&#39;IDE Builder. I ringraziamenti vanno alla comunit&agrave; GNOME per il suo lavoro a riguardo di queste nuove caratteristiche. Per maggiori informazioni, si consiglia di consultare le <a href="https://help.gnome.org/misc/release-notes/3.26/">note di rilascio originali di GNOME 3.26</a>.</p>

<p>Il nuovo rilascio evidenzia inoltre LibreOffice 5.4. La versione pi&ugrave; recente della famosa suite d&#39;ufficio offre nuove funzioni per Writer e Calc, oltre all&#39;importazioni di immagini vettoriali EMF+. &Egrave; ora possibile utilizzare chiavi OpenPGP per firmare documenti ODF.</p>

<p>Fedora 27 &egrave; reso disponibile anche mediante Fedora Media Writer, il quale consente ora di creare SD avviabili per dispositivi ARM (come Raspberry Pi). Sono stati inoltre migliorati il supporto per Windows 7 e la gestione degli screenshot. L&#39;utilit&agrave; noficia ora quando un nuovo rilascio di Fedora &egrave; disponibile. <a href="https://fedoraproject.org/wiki/How_to_create_and_use_Live_USB">Qui</a> &egrave; possibile reperire maggiori informazioni a riguardo di Fedora Media Writer.</p>

<p><br />
<strong>FEDORA ATOMIC HOST</strong></p>

<p>Di default, Fedora Atomic 27 si presenta ora come un semplice gestore di memorie container. Nello specifico, esso offre container gestibili con tecnologie Kubernet, flannel ed etcd. Tali cambiamenti introducono maggiore flessibilit&agrave; per gli utenti, che possono ora decidere se utilizzare (o meno) Kubernet. Questo rilascio contiene inoltre il nuovo rpm-ostree, che offre supporto per l&#39;override di pacchetti di base. Cockpit &egrave; inoltre aggiornato all&#39;ultima versione, che include il supporto per l&#39;installazione della Cockpit Dashboard all&#39;interno di un Atomic Host, il tutto mediante package layering.</p>

<p><br />
<strong>FEDORA SERVER</strong></p>

<p>L&#39;edizione Server di Fedora sta subendo una vasta operazione di riallestimento e riallineamento dei propri strumenti, per adeguare tutto il pacchetto al <a href="https://docs.pagure.org/modularity/">progetto modularity</a>. Tali cambiamenti consentiranno ali utenti di sfruttare un sistema operativo pi&ugrave; flessibile. I benefici includono la possibilit&agrave; di gestire al meglio una moltitudine di componenti, all&#39;interno cicli di rilascio diversi, senza lo stretto bisogno di procedere con l&#39;upgrade ogni una/due versioni di Fedora, per mantenere il sistema al passo con i tempi. La documentazione di Modularity fornisce maggiori informazioni, a riguardo di questa nuova ed emozionante concezione. Il rilascio Beta di Fedora 27 Modular Server &egrave; <a href="https://getfedora.org/server/prerelease">gi&agrave; disponibile</a>, con la previsione di un rilascio Final nell&#39;arco di circa un mese (a partire dalla data odierna).</p>

<p><br />
<strong>VARIANTI DI FEDORA E ISO A 32-BIT</strong></p>

<p>&Egrave; possibile provare una delle popolari varianti, come KDE Plasma, Xfce, ambienti desktop alternativi oppure immagini ISO per dispositivi ARM (come Raspberry Pi 2 e 3):</p>

<ul>
	<li><a href="https://spins.fedoraproject.org/">Fedora Spins</a></li>
	<li><a href="https://labs.fedoraproject.org/">Fedora Labs</a></li>
	<li><a href="https://arm.fedoraproject.org/">Fedora ARM</a></li>
	<li><a href="https://cloud.fedoraproject.org/">Fedora Cloud Base</a></li>
	<li><a href="https://alt.fedoraproject.org/">Fedora (architetture alternative)</a></li>
</ul>

<p><br />
Per quanto riguarda l&#39;architettura a 32-bit invece, non sono disponibili ISO Live e media d&#39;installazione per Workstation, Labs e Spin. Sono invece disponibili le immagini &quot;<a href="https://alt.fedoraproject.org/">network-installation&quot;</a> per i rilasci a 32-bit di Fedora Workstation ed Everything.</p>

<p><br />
<strong>NOTE DI RILASCIO</strong></p>

<p>Per maggiori informazioni a riguardo dei cambiamenti riguardanti Fedora 27, &egrave; possibile consultare le <a href="https://docs.fedoraproject.org/f27/release-notes/">note di rilascio</a> contenute nel <a href="https://docs.fedoraproject.org/">nuovo portale dedicato alla documentazione di Fedora</a>.</p>
