---
categories:
- staff news
layout: ultime_news
title: '03/11/2015: Rilascio di Fedora 23 Final (ore 16:00 italiane)!!!'
created: 1446502649
---
<p><img alt="" src="https://fedoraproject.org/w/uploads/0/06/F23-alpha.jpg" style="width: 200px; height: 150px;" /></p>
<p class="rtejustify">Oramai ci siamo! Sta arrivando il grande giorno di Fedora 23!</p>
<p class="rtejustify">Da domani, <strong>Marted&igrave; 03 Novembre 2015 (ore 16:00 italiane)</strong> sar&agrave; possibile scaricare il nuovo rilascio, direttamente da <a href="https://getfedora.org">https://getfedora.org</a>.</p>
<p class="rtejustify">Tra i cambiamenti lato utente spiccano le seguenti novit&agrave;:</p>
<ul>
	<li class="rtejustify">
		Gnome 3.18.</li>
	<li class="rtejustify">
		KDE Plasma 5.4.0.</li>
	<li class="rtejustify">
		LibreOffice 5.0.0.</li>
	<li class="rtejustify">
		Nuova spin Cinnamon.</li>
	<li class="rtejustify">
		Nuovo sistema di avanzamento di versione (dnf-plugin-system-upgrade).</li>
	<li class="rtejustify">
		Standardizzazione requisiti complessit&agrave; password all&#39;interno del sistema</li>
	<li class="rtejustify">
		Modifiche nel ciclo di rilascio del prodotto Atomic.</li>
</ul>
<p class="rtejustify">Di seguito, alcuni link utili:</p>
<ul>
	<li class="rtejustify">
		Lista completa cambiamenti: <a href="https://fedoraproject.org/wiki/Releases/23/ChangeSet">https://fedoraproject.org/wiki/Releases/23/ChangeSet</a></li>
	<li class="rtejustify">
		Elenco bug comuni: <a href="https://fedoraproject.org/wiki/Common_F23_bugs">https://fedoraproject.org/wiki/Common_F23_bugs</a></li>
	<li class="rtejustify">
		Guida all&#39;installazione (valida anche per Fedora 23): <a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a></li>
	<li class="rtejustify">
		Guida all&#39;avanzamento di versione: <a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a></li>
</ul>
