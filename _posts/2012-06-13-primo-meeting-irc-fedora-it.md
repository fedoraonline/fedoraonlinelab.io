---
categories:
- staff news
layout: ultime_news
title: 'Primo Meeting IRC #fedora-it'
created: 1339587352
---
<p>
	Sono molto felice di questa novit&agrave;, un meeting della comunit&agrave; italiana di Fedora su canale IRC, aperto a tutti gli utenti di Fedora di lingua italiana.<br />
	E&#39; in programma di instaurare dei meeting con frequenza regolare, prima con annunci specifici per ogni incontro, e poi con un automatismo di &quot;abitudine anche settimanale&quot;.</p>
<p>
	Gli obiettivi dei meeting IRC sono molteplici:</p>
<ul>
	<li>
		Maggiore partecipazione degli utenti italiani di Fedora nelle attivit&agrave; della comunit&agrave;</li>
	<li>
		Discussione con vari membri dei gruppi del fedoraproject su novit&agrave; e problematiche</li>
	<li>
		Confronto e aiuto per chi volesse partecipare pi&ugrave; attivamente al Progetto Fedora</li>
	<li>
		ecc ecc</li>
</ul>
<p>
	Il primo meeting, cos&igrave; come i successivi per adesso, si terr&agrave; sul canale <strong><span class="bbu">#fedora-it</span></strong> su <a href="http://freenode.net">freenode</a> e precisamente:</p>
<p style="text-align: center">
	<strong>Gioved&igrave; 21 giugno 2012<br />
	ore 22</strong></p>
<p>
	La durata prevista &egrave; di un&#39;ora (chiusura meeting alle 23:00) e l&#39;ordine del giorno &egrave; il seguente:</p>
<ul>
	<li>
		Movimento Fedora in Italia: progetti e opportunit&agrave;</li>
	<li>
		Boutique: stato attuale e proposte organizzative</li>
</ul>
<p>
	Per partecipare potete usare:</p>
<ol class="decimal">
	<li>
		<a href="http://webchat.freenode.net/">Webchat</a></li>
	<li>
		Un chat client, come per esempio xchat (per KDE esiste p.e. konversation):
		<div class="codebox">
			<pre>
<code># yum install xchat</code></pre>
		</div>
		<ul>
			<li>
				Avviare xchat da Applicazioni --&gt; Internet --&gt; IRC</li>
			<li>
				Digitare Nickname, Username e nome reale</li>
			<li>
				Tra le reti disponibili scegliere FreeNode --&gt; irc.freenode.net</li>
			<li>
				Digitare il canale a cui si vuole partecipare (#fedora-it)</li>
		</ul>
	</li>
</ol>
<p>
	Per ulteriori domande o dubbi potete postare in questa discussione.<br />
	<a href="http://forum.fedoraonline.it/viewtopic.php?id=18379">http://forum.fedoraonline.it/viewtopic.php?id=18379</a><br />
	Spero di vedervi partecipare numerosi.<br />
	Robyduck</p>
