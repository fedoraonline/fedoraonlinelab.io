---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 17 - Beefy Miracle
created: 1338300273
---
<p>
	E&#39; stata appena rilasciata ufficialmente Fedora 17, scaricabile da qui:<br />
	<a href="http://get.fedoraproject.org/">http://get.fedoraproject.org/</a></p>
<p>
	Come di consueto consigliamo la lettura di bug noti, che, si spera, vengano risolti man mano in queste prime settimane di vita:<br />
	<a href="http://fedoraproject.org/wiki/Common_F17_bugs">http://fedoraproject.org/wiki/Common_F17_bugs</a></p>
<p>
	Caratteristiche:</p>
<ul>
	<li>
		Gnome 3.4</li>
	<li>
		Juno</li>
	<li>
		OpenJDK 7</li>
	<li>
		Php 5.4</li>
	<li>
		Kernel 3.3.4</li>
	<li>
		Supporto GMA (poulsbo)</li>
	<li>
		Supporto Broadcom</li>
	<li>
		Firewalld</li>
</ul>
<p>
	ecc ecc, ulteriori informazioni sono disponibili qui:<br />
	<a href="http://fedoraproject.org/wiki/Releases/17/FeatureList">http://fedoraproject.org/wiki/Releases/17/FeatureList</a></p>
<p>
	Il filesystem di default &egrave; rimasto ext4, in quanto btrfs &egrave; slittato almeno alla versione 18. Inoltre Gnome-Shell &egrave; in grado di funzionare anche con quelle schede grafiche che non supportano l&#39;accelerazione 3D, un enorme passo avanti per quanto riguarda l&#39;usabilit&agrave; del DE di default.</p>
<p>
	Da notare che Fedora 17 include anche il progetto &quot;Usrmove&quot;, ovvero lo spostamento di tutte le /lib e /bin sotto la directory /usr. Di fatto, le directory /lib, /lib64, /bin e /sbin ora contengono soltanto dei link simbolici ai file che si trovano sotto la directory /usr.</p>
<p>
	Le note di rilascio ufficiali sono disponibili qui: <a href="http://fedorapeople.org/groups/docs/release-notes/it-IT/">http://fedorapeople.org/groups/docs/release-notes/it-IT/</a></p>
