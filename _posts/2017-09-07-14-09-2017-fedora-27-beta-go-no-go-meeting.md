---
categories:
- staff news
layout: ultime_news
title: '14/09/2017: Fedora 27 Beta go/no-go meeting'
created: 1504789809
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Gioved&igrave; 14 Settembre 2017, alle 19:00 (ore italiane), si terr&agrave; il Go/No-Go Meeting Internazionale. Il luogo &egrave; il canale &quot;#fedora-meeting-2&quot;, sul server IRC Freenode.<br />
	<br />
	Prima di ogni rilascio ufficiale, i team Development, QA e Release Engineering, si incontrano per stabilire se determinati criteri qualitativi sono stati rispettati. La riunione &egrave;, ovviamente aperta a tutti.<br />
	<br />
	La disponibilit&agrave; di una RC (Release Canditate) e un certo livello di copertura garantita da test, sono prerequisiti essenziali. Se il tutto non sar&agrave; pronto per la data indicata, si discuter&agrave; ulteriormente a riguardo dei bug bloccanti (blocker bugs) e si programmeranno le azioni future.<br />
	&nbsp;<br />
	<br />
	Link utili:<br />
	- Evento su calendario Fedocal: <a href="https://apps.fedoraproject.org/calendar/meeting/6448/?from_date=2017-09-11">https://apps.fedoraproject.org/calendar/meeting/6448/?from_date=2017-09-11</a><br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio: <a href="https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria">https://fedoraproject.org/wiki/Fedora_27_Beta_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/27/beta/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/27/beta/buglist</a><br />
	- Date previste per i rilasci: <a href="https://fedoraproject.org/wiki/Releases/27/Schedule">https://fedoraproject.org/wiki/Releases/27/Schedule</a></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">&nbsp;</p>
