---
categories:
- staff news
layout: ultime_news
title: 30/10/2015 - Terzo Fedora 23 Go/No-go Meeting (ore 17.00)
created: 1446141565
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Durante il Go/No-go di oggi, &egrave; stato nuovamente deciso di non dare il via libera a Fedora 23. Verr&agrave; riconvocato un altro meeting per la data di domani.</p>
<p>La causa &egrave; la presenza del seguente bug (accettato come blocker), ancora da risolvere:</p>
<ul>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1276165">https://bugzilla.redhat.com/show_bug.cgi?id=1276165</a></li>
</ul>
<p>Sar&agrave; presto resa disponibile un&#39;altra Release Candidate contenente la risoluzione del problema segnalato.</p>
<p>Qui il log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-29/f23-final-go_no_go-meeting_2.2015-10-29-16.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-29/f23-final-go_no_go-meeting_2.2015-10-29-16.00.log.html</a></p>
<p>Appuntamento a Venerd&igrave; 30 (ore 17.00 italiane) per un altro Go/No-go!!!</p>
<p>&nbsp;</p>
