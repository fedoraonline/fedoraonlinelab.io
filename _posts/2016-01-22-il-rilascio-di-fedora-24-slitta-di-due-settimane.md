---
categories:
- staff news
layout: ultime_news
title: Il rilascio di Fedora 24 slitta di due settimane!
created: 1453445759
---
<div data-contents="true">
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="ecvje-0-0">
		<img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 70px; height: 70px;" /></div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="ecvje-0-0">
		&nbsp;</div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="ecvje-0-0">
		&nbsp;</div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="ecvje-0-0">
		<span data-offset-key="ecvje-0-0"><span data-text="true">&Eacute; stato deciso di includere GCC6 in Fedora 24 e questo necessit&agrave; di un Mass Rebuild, non originariamente previsto per questo rilascio. </span></span></div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="6pkmn-0-0">
		<span data-offset-key="6pkmn-0-0"><span data-text="true">Questo comporta lo slittamento di Fedora di due settimane. Le nuove date previste, sono le seguenti:</span></span></div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="c3d4g-0-0">
		&nbsp;</div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="srjv-0-0">
		<span data-offset-key="srjv-0-0"><span data-text="true">2016-03-15 Alpha Release</span></span></div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="ad3ea-0-0">
		<span data-offset-key="ad3ea-0-0"><span data-text="true">2016-04-26 Beta Release</span></span></div>
	<div class="_45m_ _2vxa" data-block="true" data-offset-key="knll-0-0">
		<span data-offset-key="knll-0-0"><span data-text="true">2016-05-31 Fedora 24 Final Release (GA)﻿</span></span></div>
</div>
<p>&nbsp;</p>
