---
categories:
- staff news
layout: ultime_news
title: Disponibile Fedora 18 Alpha Test Compose 1 (TC1)
created: 1344849193
---
<p>
	Riportiamo l&#39;annuncio apparso sulla mailing list test-announce di Fedora.</p>
<p>
	Coma da programma Fedora 18 Alpha Test Compose 1 (TC1) &egrave; ora disponibile per il test. (La versione live non &egrave; ancora disponibile).<br />
	Tutte le informazioni, comprese le modifice, si possono trovare a questo indirizzo https://fedorahosted.org/rel-eng/ticket/5284 .</p>
<p>
	Si prega di leggere le seguenti pagine per i link di download (incluse le immagini ISO della versione delta) e per le instruzioni di testing. Normalmente dl.fedoraproject.org dovrebbe fornire un download veloce ma &egrave; disponibile il mirror download-ib01.fedoraproject.org (con un ritardo di circa 1 ora) in caso di problemi. Per utilizzarlo basta sostituire &quot;dl&quot; con &quot;download-ib01&quot; nell&#39;URL per il download.<br />
	<br />
	Installazione:<br />
	<br />
	https://fedoraproject.org/wiki/Test_Results:Current_Installation_Test<br />
	<br />
	Base:<br />
	<br />
	https://fedoraproject.org/wiki/Test_Results:Current_Base_Test<br />
	<br />
	Desktop:<br />
	<br />
	https://fedoraproject.org/wiki/Test_Results:Current_Desktop_Test</p>
<p>
	Idealmente, tutte le priorit&agrave; per i test dei caso Alpha per Installazione [2], Base [3] e Desktop [4] devono passare per rispettare i criteri della versione Alpha [5]. Maggior aiuto &egrave; disponibile sul canale #fedora-qa di irc.freenode.net [6], o sulla mailing-list test [7].<br />
	<br />
	Create Fedora 18 Alpha test compose (TC) and release candidate (RC)<br />
	https://fedorahosted.org/rel-eng/ticket/5284<br />
	<br />
	F18 Alpha Blocker tracker bug:<br />
	https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=752654<br />
	<br />
	F18 Alpha Nice-To-Have tracker bug:<br />
	https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=752662<br />
	<br />
	[1] http://rbergero.fedorapeople.org/schedules/f-18/f-18-quality-tasks.html<br />
	[2] https://fedoraproject.org/wiki/QA:Installation_validation_testing<br />
	[3] https://fedoraproject.org/wiki/QA:Base_validation_testing<br />
	[4] https://fedoraproject.org/wiki/QA:Desktop_validation_testing<br />
	[5] https://fedoraproject.org/wiki/Fedora_18_Alpha_Release_Criteria<br />
	[6] irc://irc.freenode.net/fedora-qa<br />
	[7] https://admin.fedoraproject.org/mailman/listinfo/test</p>
