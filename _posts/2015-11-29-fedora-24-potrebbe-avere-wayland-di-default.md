---
categories:
- staff news
layout: ultime_news
title: Fedora 24 potrebbe avere Wayland di default
created: 1448788816
---
<p class="rtecenter"><img alt="" src="https://fedoramagazine.org/wp-content/uploads/2015/08/wayland-945x400.png" style="width: 473px; height: 200px;" /></p>
<p>&nbsp;</p>
<p>Fedora &egrave; sempre tra le prime distribuzioni GNU/Linux a proporre le nuove tecnologie.</p>
<p>Per questo, il Workstation Working Group ha annunciato un piano decisamente rivoluzionario, che coinvolge direttamente l&#39;esperienza utente.</p>
<p>L&#39;idea &egrave; quella di rendere la sessione Wayland come predefinita, in Fedora 24 Workstation. Questo ovviamente avverr&agrave; solo nel caso in cui la nuova tecnologia si dimostrer&agrave; sufficientemente stabile.&nbsp; L&#39;attuale sessione Xorg, sar&agrave; comunque mantenuta come ripiego. Tale cambiamento &egrave; gi&agrave; stato applicato nella versione di sviluppo di Fedora.</p>
<p>Da questo punto di vista, gli utenti giocheranno un ruolo fondamentale nel ciclo di testing. Dato che la nuova tecnologia coinvolge il comparto grafico, &egrave; assai importante ricevere numerosi feedback relativi al suo comportamento, sopratutto in merito a possibili bug. Essendo tale sessione gi&agrave; presente su Fedora 22 e Fedora 23, l&#39;invito &egrave; quindi rivolto a tutti e non solo agli utilizzatori di Fedora Rawhide.</p>
<p>&nbsp;</p>
<p><strong>Link utili</strong>:</p>
<p>Articolo Fedora magazine: <a href="https://fedoramagazine.org/help-fedora-test-wayland/">https://fedoramagazine.org/help-fedora-test-wayland/</a></p>
<p>Guida al debug dei problemi relativi a Wayland: <a href="https://fedoraproject.org/wiki/How_to_debug_Wayland_problems">https://fedoraproject.org/wiki/How_to_debug_Wayland_problems</a></p>
