---
categories:
- staff news
layout: ultime_news
title: Nuove pagine social per Fedora Online!
created: 1443507285
---
<p><img alt="" src="https://pbs.twimg.com/profile_images/1847926419/folsn_400x400.png" style="width: 100px; height: 100px;" /></p>
<p>Come da titolo, stiamo assistendo al rinnovamento delle pagine dei social network legate a Fedora Online.</p>
<p>&nbsp;</p>
<p>Per link, discussioni, dubbi, consigli e richieste, esiste una discussione apposita, aperta nella sezione &quot;Annunci e novit&agrave;&quot;:</p>
<p>http://forum.fedoraonline.it/viewtopic.php?pid=232517#p232517</p>
