---
categories:
- staff news
layout: ultime_news
title: 'Fedora 20 è pronta: rilascio il 17 dicembre!'
created: 1386882328
---
<p>La prima Release Candidate &egrave; gi&agrave; quella buona. Dopo un meeting lunghissimo di 3:15 (!) &egrave; stato deciso pochi minuti fa di dare la benedizione al rilascio di Fedora 20 per il 17 dicembre. Pienamente in tempo con la schedule originaria -infatti solo alla Beta si era deciso di voler anticipare addirittura il rilascio di una settimana- Heisenbug vedr&agrave; la luce giusto in tempo per un felice Natale.</p>
<p>L&#39;annucio ufficiale &egrave; reperibile qui: <a href="https://lists.fedoraproject.org/pipermail/logistics/2013-December/001748.html">https://lists.fedoraproject.org/pipermail/logistics/2013-December/001748.html</a></p>
<p>Log del meeting: <a href="http://bit.ly/19kCBzj">http://bit.ly/19kCBzj</a></p>
