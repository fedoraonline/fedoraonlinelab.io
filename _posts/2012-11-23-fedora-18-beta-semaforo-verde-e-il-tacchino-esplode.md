---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18 Beta: semaforo verde! ... e il tacchino esplode!'
created: 1353659456
---
<p>
	Ieri sera, alla fine di un meeting molto intenso e dopo aver valutato ogni singolo aspetto insieme ai singoli gruppi di sviluppo, la Beta di Fedora 18 ha finalmente ottenuto il via libera e verr&agrave; rilasciata ufficialmente</p>
<p class="rtecenter">
	<strong>marted&igrave; 27 novembre 2012</strong></p>
<p>
	La Beta porter&agrave; il nome &quot;Exploding Turkey&quot;, in seguito al simpatico aneddoto riguardante proprio il meeting di ieri. Infatti&nbsp;nel giorno&nbsp;in cui gli sviluppatori europei avevano fissato il meeting, negli Stati Uniti era in corso la festa pi&ugrave; importante dell&#39;anno dopo Natale, ovvero il &quot;Thanksgiving Day&quot;. Ovviamente ci sono state proteste da parte degli amici americani per la scelta del giorno, ma alla fine &egrave; prevalsa la voglia di far nascere finalmente la Beta.</p>
<p>
	C&#39;era chi si era collegato dal telefono in viaggio o gi&agrave; a tavola, o chi invece stava ancora preparando la cena insieme alla famiglia, ovviamente cucinando il tacchino. Da qui il nome per la Beta di Fedora 18, sperando che nessuno si sia distratto troppo ritrovandosi per davvero un tacchino esploso nel forno :)</p>
<p>
	Fonte: <a href="http://meetbot.fedoraproject.org/fedora-meeting-1/2012-11-22/f18_beta_gono-go_meeting.2012-11-22-20.01.log.html">http://meetbot.fedoraproject.org/fedora-meeting-1/2012-11-22/f18_beta_gono-go_meeting.2012-11-22-20.01.log.html</a></p>
