---
categories: []
layout: page
title: Fedora 20 - Un sistema Desktop a portata di tutti
created: 1409475096
---
<p style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);"><a href="http://bitbiz.org/robyduck/?p=13"><img alt="" src="http://www.fedoraonline.it/sites/all/themes/newfol20/images/fedora20.png" style="width: 200px; height: 283px;" /></a></p>
<p style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">Fedora &egrave; una distribuzione Linux tra le pi&ugrave; utilizzate al mondo e ha appena compiuto dieci anni; sponsorizzata da RedHat, la versione 20 offre una potenzialit&agrave; a molti ignota e contiene le versioni pi&ugrave; aggiornate delle varie applicazioni.<br />
	Questo e-book vuole accompagnare il lettore durante l&rsquo;installazione e la configurazione del sistema fino ad arrivare a muovere i primi passi in modo autonomo. Molte immagini ed esempi rendono le guide molto intuibili e permettono di orientarsi facilmente durante ogni passaggio. L&rsquo;utilizzo, dove possibile, della sola interfaccia grafica fa di questo testo lo strumento perfetto per chi si avvicina per la prima volta al mondo Linux.</p>
<p style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">Il libro &egrave; di circa 130 pagine e affronta i seguenti argomenti:</p>
<ul>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Prefazione</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Benvenuto su Linux</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Installazione</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Primi passi su Fedora</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Gestione dei pacchetti RPM</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Ambienti Desktop</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		La Shell e i suoi comandi</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Configurazione delle schede video</li>
	<li style="margin: 0px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Struttura e gerarchia del filesystem</li>
	<li style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">
		Applicazioni e plugin</li>
</ul>
<p style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);">Infine &egrave; da sottolineare che il ricavato di questo e-book viene utilizzato per il mantenimento dell&rsquo;infrastruttura di&nbsp;<a href="http://www.fedoraonline.it/" style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; font-weight: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0);">fedoraonline.it</a>, la comunit&agrave; italiana degli utenti di Fedora.</p>
<p style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);"><strong style="margin: 0px; padding: 0px; border: 0px; font-family: inherit; font-size: inherit; font-style: inherit; font-variant: inherit; line-height: inherit; vertical-align: baseline;">Prezzo: 4,00 &euro;</strong><br />
	Formati: epub, kindle</p>
<p style="margin: 0px 0px 30px; padding: 0px; border: 0px; font-family: Comfortaa, Helvetica-Neue, Sans; font-size: 16px; line-height: 28px; vertical-align: baseline; color: rgb(131, 133, 133);"></p>
<div id="imgblock">
	<ul>
		<li class="imgupload">
			<span><a href="http://bitbiz.org/robyduck/?p=13">EPUB</a> </span></li>
		<li class="imgupload">
			<span><a href="http://www.amazon.it/Fedora-20-sistema-Desktop-portata-ebook/dp/B00N6XR15U/ref=sr_1_2?s=digital-text">KINDLE</a> </span></li>
	</ul>
</div>
<p>&nbsp;</p>
