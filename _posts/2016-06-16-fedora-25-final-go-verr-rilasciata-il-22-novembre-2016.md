---
categories:
- staff news
layout: ultime_news
title: 'Fedora 25 Final è GO: verrà rilasciata il 22 Novembre 2016!'
created: 1466105035
---
<p class="rtejustify"><img alt="" src="http://qalab.altervista.org/folnews/f25.png" style="width: 384px; height: 240px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Nel corso del Go/No-Go Meeting di ieri, relativo a Fedora 25 Final, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.</span></p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Fedora 25 Final sar&agrave; resa quindi pubblicamente disponibile il giorno 22 Novembre 2016.</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2016-11-17/f25-final-gono-go-meeting.2016-11-17-17.00.log.html" style="white-space: normal; word-break: break-all; font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">https://meetbot.fedoraproject.org/fedora-meeting-2/2016-11-17/f25-final-gono-go-meeting.2016-11-17-17.00.log.html</a></span></p>
<p class="rtejustify">&nbsp;</p>
