---
categories:
- staff news
layout: ultime_news
title: File system - Riflessioni e novità
created: 1468074611
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2014/07/500px-Tux.svg_.png" style="width: 100px; height: 116px;" /></p>
<p class="rtejustify">Recentemente, l&#39;ambassador italiano Francesco &quot;Frafra&quot; Frassinelli, ha aperto un interessante topic all&#39;interno di Fedora Online.</p>
<p class="rtejustify">Lo scopo dell&#39;intervento, &egrave; quello di riassumere le principali caratteristiche dei file system btrfs, ext4, xfs e bcache. Il link &egrave; il seguente: <a href="http://forum.fedoraonline.it/viewtopic.php?pid=238302">http://forum.fedoraonline.it/viewtopic.php?pid=238302</a></p>
