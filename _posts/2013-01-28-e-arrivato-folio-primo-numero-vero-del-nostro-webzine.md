---
categories:
- staff news
layout: ultime_news
title: E' arrivato Folio, primo numero "vero" del nostro webzine
created: 1359386900
---
<div class="postmsg">
	<p>Ragazzi ci siamo!<br />
		E&#39; stato appena rilasciato per il download il primo vero numero di Folio, dopo che la versione prova dell&#39;anno scorso aveva sucitato molto interesse.<br />
		Molte ore di lavoro abbiamo trascorso dietro al PC e contando sulla collaborazione di alcuni utenti ne &egrave; venuto fuori una bella raccolta di articoli di 51 pagine. Ce n&#39;&egrave; per tutti i gusti e speriamo di poter continuare questa esperienza con un aiuto pi&ugrave; massiccio da parte della comunit&agrave;.<br />
		Vorrei ri-ringraziare mailga per l&#39;ottimo lavoro svolto e per essere riuscito a stare nei tempi che ci eravamo prefissati. E&#39; chiaro che siamo aperti a critiche e proposte per migliorare, soprattutto se sono costruttive e fattibili (se poi ci offrite anche il vostro contributo per mettere le proposte in pratica tanto meglio).<br />
		Il webzine &egrave; disponibile come gi&agrave; il primo numero dalla nostra <a href="http://doc.fedoraonline.it/Folio">pagina dedicata a Folio</a>, quindi non ci rimane che augurarvi una buona lettura.</p>
	<p>I vostri feedback sono importanti per noi, non siate timidi e lasciate un vostro commento, ma soprattutto &quot;parlatene&quot; in giro.</p>
	<p>Lo Staff di Fedora Online</p>
</div>
<p>&nbsp;</p>
