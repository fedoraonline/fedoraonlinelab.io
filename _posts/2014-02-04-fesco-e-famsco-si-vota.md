---
categories:
- staff news
layout: ultime_news
title: 'FESCo e FAmSCo: si vota!'
created: 1391505075
---
<p><strong>Da oggi, 4 febbraio, e fino alle 23:59 UTC del 10 febbraio</strong>, sono aperte le <strong>elezioni al FESCo e al FAmSCo</strong>!</p>
<p>Tutti i contributor che siano almeno in un gruppo oltre al CLA (CLA+1) possono votare e decidere quindi chi dovr&agrave; far parte dei due comitati per i prossimi due cicli di rilascio (F21 e F22). Per il FAmSCo si vota per 3 seggi, il FESCo invece avr&agrave; 4 seggi da assegnare.</p>
<p>E&#39; importante che chi ha i requisiti partecipi alle votazioni:&nbsp;<a href="https://fedoraproject.org/wiki/Elections#Committee_Elections_Schedule">https://fedoraproject.org/wiki/Elections#Committee_Elections_Schedule</a></p>
