---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 23 Beta!
created: 1442932676
---
<p><img alt="" src="https://fedoraproject.org/w/uploads/f/f0/F23_beta_gnokii.png" style="width: 200px; height: 100px; float: left; margin-left: 10px; margin-right: 10px;" /></p>
<p>Rilasciata pochi minuti fa la versione Beta di Fedora 23.</p>
<p>Aspettando la versione finale prevista per il giorno 27 ottobre 2015 possiamo gi&agrave; da ora scaricare ed installare questa release che seppur ancora potrebbe presentare qualche bug si preannuncia essere molto interessante.</p>
<p>&nbsp;</p>
<p>Le novit&agrave; pi&ugrave; importanti riguardanti Fedora workstation e gi&agrave; presenti nella Beta sono l&#39;anteprima della nuovissima versione di Gnome, gli aggiornamenti di Wayland con <span id="result_box" lang="it"><span title="- Support for ambient backlight drivers, so brightness responds to
&nbsp;&nbsp;&nbsp;&nbsp;">il supporto HiDPI misto, il supporto per la retroilluminazione ambientale (utile nei laptop), l&#39;applicazione Software migiorata, il supporto <span id="result_box" lang="it"><span title="- Refreshed support for Google APIs to provide access to user data
&nbsp;&nbsp;&nbsp;&nbsp;">per le API di Google per fornire l&#39;accesso ai dati degli utenti&nbsp;</span><span title="through GNOME apps (including Google Drive integration)

">attraverso applicazioni Gnome (es. Google Drive), e infine, ma non ultimo Libreoffice alla versione 5.</span></span></span></span></p>
<p>L&#39;ultima release della nuova Fedora 23 pu&ograve; essere scaricata direttamente oppure tramite torrent seguendo i link dal sito ufficiale Fedora: https://getfedora.org/</p>
<p>E naturalmente non dimentichiamoci di leggere l&#39;annuncio ufficiale di <span class="e" id=":rp" tabindex="0" title="Dennis Gilmore">Dennis Gilmore:</span></p>
<p>https://lists.fedoraproject.org/pipermail/announce/2015-September/003289.html</p>
<p>&nbsp;</p>
<p>Enjoy!</p>
