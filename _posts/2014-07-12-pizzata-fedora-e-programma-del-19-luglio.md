---
categories:
- staff news
layout: ultime_news
title: Pizzata Fedora e programma del 19 luglio
created: 1405198269
---
<div>
	<span style="font-size:12px;"><span style="font-family:tahoma,geneva,sans-serif;">Come probabilmente sapete, il prossimo sabato, 19 luglio, la comunit&agrave; Fedora italiana organizza una serata a Milano, in cui non solo si parler&agrave; di Fedora ma si faranno anche alcune sessioni pratiche e soprattutto divertenti. Ovviamente non mancheranno pizza e birra :)</span></span></div>
<div>
	<span style="font-size:12px;"><span style="font-family:tahoma,geneva,sans-serif;">Di seguito un piccolo vademecum per la serata, chi volesse partecipare pu&ograve; ancora aderire ma affrettatevi perch&egrave; i posti vanno prenotati.</span></span></div>
<div>
	&nbsp;</div>
<div id="magicdomid6" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd b" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><b>Quando?</b></span><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;sabato 19 luglio 2014</span></span></div>
<div id="magicdomid7" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd b" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><b>Dove?</b></span><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;Ristorante Barbagia, via Ruggero di Lauria 18, Milano</span></span></div>
<div id="magicdomid8" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd b" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><b>Modulo di adesione</b></span><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">:&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="http://ur1.ca/hqe5h" style="cursor: pointer !important;">http://ur1.ca/hqe5h</a></span></span></div>
<div id="magicdomid9" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd b" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><b>Topic:</b></span><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="http://forum.fedoraonline.it/viewtopic.php?pid=225064" style="cursor: pointer !important;">http://forum.fedoraonline.it/viewtopic.php?pid=225064</a></span></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<strong><span style="font-family:tahoma,geneva,sans-serif;">Informazioni aggiuntive pizzeria</span></strong></div>
<div id="magicdomid12" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp; Mappa (libera):&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="http://osm.org/go/0CjFCaP~1?m=" style="cursor: pointer !important;">http://osm.org/go/0CjFCaP~1?m=</a></span></span></div>
<div id="magicdomid13" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp; Mappa (proprietaria):&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="https://goo.gl/maps/vNFV2" style="cursor: pointer !important;">https://goo.gl/maps/vNFV2</a></span></span></div>
<div id="magicdomid14" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp; TripAdvisor:&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="http://ur1.ca/hqe5f" style="cursor: pointer !important;">http://ur1.ca/hqe5f</a></span></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><b>Tragitto verso pizzeria</b></span></div>
<div id="magicdomid22" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div id="magicdomid23" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">1. Usciti da Milano Centrale, prendere via Galvani sulla destra</span></span></div>
<div id="magicdomid24" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">2. A piedi fino a via Melchiorre Gioia, 51-55 (5 minuti)</span></span></div>
<div id="magicdomid25" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">3. Tram, linea 43 (19 fermate, 25 minuti)</span></span></div>
<div id="magicdomid26" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;&nbsp;&nbsp;&nbsp; Info:&nbsp;</span><span class="author-g-ciwdl20fera4dyjd url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="http://ur1.ca/hpzb0" style="cursor: pointer !important;">http://ur1.ca/hpzb0</a></span></span></div>
<div id="magicdomid27" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;&nbsp;&nbsp;&nbsp; Tempi di attesa (sabato): &lt;12 minuti</span></span></div>
<div id="magicdomid28" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;&nbsp;&nbsp;&nbsp; Ultima corsa: 0:50</span></span></div>
<div id="magicdomid29" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;&nbsp;&nbsp;&nbsp; Costo: 1,50 &euro; (biglietto valido per 90 minuti)</span></span></div>
<div id="magicdomid30" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">4. A piedi fino a Via Ruggero di Lauria, 18 (2 minuti)</span></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><strong><span class="author-g-ciwdl20fera4dyjd" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">Attivit&agrave;:</span></strong></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;">a) stato di salute della comunit&agrave; Fedora in Italia.</span></div>
<div id="magicdomid39" style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;"><span class="author-g-iq6p8cjun13n7973" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">b) gpg key signin party&nbsp;</span><span class="author-g-iq6p8cjun13n7973 url" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;"><a href="https://www.gnupg.org/howtos/it/keysigning_party.html" style="cursor: pointer !important;">https://www.gnupg.org/howtos/it/keysigning_party.html</a></span><span class="author-g-iq6p8cjun13n7973" style="cursor: auto; padding-top: 1px; padding-bottom: 1px;">&nbsp;[?]</span></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;">c) Quiz Fedora</span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;">Informazioni per il GPG key signing party:</span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;">Una guida in inglese sulle chiavi GPG, come crearne una e inviarla sul server si trova qui:&nbsp;<a href="http://fedoraproject.org/wiki/Creating_GPG_Keys" style="font-family: Arial, Verdana, sans-serif; font-size: 12px;">http://fedoraproject.org/wiki/Creating_GPG_Keys</a></span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	<span style="font-family:tahoma,geneva,sans-serif;">E&#39; buona norma creare le proprie chiavi GPG prima di presentarsi al party e di portare con se dei bigliettini con il proprio nome, cognome, mail e fingerprint. Se si ha difficolt&agrave; nella creazione delle chiavi si pu&ograve; sempre portare il proprio PC e fare tutto al volo.</span></div>
<div style="padding-right: 1px; color: rgb(0, 0, 0); font-family: monospace; font-size: 13px; line-height: 17px;">
	&nbsp;</div>
