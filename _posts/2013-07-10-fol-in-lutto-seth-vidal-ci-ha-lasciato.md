---
categories:
- staff news
layout: ultime_news
title: 'FOL in lutto: Seth Vidal ci ha lasciato'
created: 1373441700
---
Abbiamo perso un pezzo di Fedora, uno che con la sua competenza ha saputo dare molte svolte all'interno di Fedora e che ora non c'è piú.
Seth Vidal (skvidal) è stato teamleader di Yum e ha contribuito a CentOS, ultimamente era una presenza costante nell'infrastruttura di Fedora. A lui dobbiamo molto, Fedora non sarebbe quella che è adesso se lui non avesse messo del suo e tutte le volte che usiamo Yum dovremmo essere grati.

Al di lá delle competenze peró perdiamo soprattutto un amico, disponibile a qualsiasi ora e mai arrogante. Una persona meravigliosa che stava agli scherzi e ne faceva anche agli altri. Trattava tutti allo stesso modo, con la stessa gentilezza e positivitá.

FOL per tutta questa settimana si vestirá di lutto per ricordare Seth e la sua passione per Fedora.

Il comunicato ufficiale da parte della Project Leader è possibile leggerlo qui:
https://lists.fedoraproject.org/pipermail/announce/2013-July/003174.html
