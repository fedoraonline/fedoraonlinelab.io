---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 19 - Schrödinger's Cat
created: 1372795806
---
<p>Aprite la scatola e guardate bene!&nbsp;<span style="font-family: arial, sans-serif; font-size: 13px;">Schr&ouml;dinger&#39;s Cat &egrave; VIVO!!!</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">Rilasciata oggi con un ritardo proveniente dalla pre-release di una sola settimana una delle pi&ugrave; importanti versioni di Fedora, perch&egrave; introduce molte novit&agrave; e soprattutto consolida le altrettante novit&agrave; introdotte con la F18. Come sempre le Note di Rilascio sono una delle prime fonti da leggere per informarsi sull&#39;ultima creazione in casa Fedora:</span></p>
<p><a href="http://docs.fedoraproject.org/en-US/Fedora/19/html/Release_Notes/">http://docs.fedoraproject.org/en-US/Fedora/19/html/Release_Notes/</a></p>
<p>Novit&agrave;? Tantissime, a partire da due nuove Spin, MATE-Compiz e Jam-KDE, una spin dedicata agli appassionati della musica. Inoltre &egrave; di nuovo stata resa disponibile la Games Spin, che era stata saltata con Fedora 18. Tutte le novit&agrave; sono reperibili qui:</p>
<p><a href="http://fedoraproject.org/wiki/Releases/19/FeatureList">http://fedoraproject.org/wiki/Releases/19/FeatureList</a></p>
<p>Tra le quali:</p>
<ul>
	<li>
		Modellamento 3D e stampe</li>
	<li>
		Openshift come PaaS</li>
	<li>
		Ruby 2.0</li>
	<li>
		MariaDB come database di default</li>
	<li>
		GNOME 3.8</li>
	<li>
		KDE 4.10</li>
	<li>
		ecc. ecc.</li>
</ul>
<p>Provare per credere! E&#39; inoltre gi&agrave; trapelata la data di rilascio per Fedora 20, dovrebbe essere a met&agrave; novembre, ma per ora gustiamoci il nostro gattino...</p>
