---
categories:
- staff news
layout: ultime_news
title: Fedora 21 Beta slitta di...
created: 1414096627
---
<p>di solito il titolo si conclude con &quot;una settimana&quot;, ma stavolta si vuole provare a farla slittare <strong>di un solo giorno</strong>. Decisione rimandata quindi a domani!</p>
<p>Anaconda ha rilasciato stamattina una nuova versione che dovrebbe risolvere molte problematiche, il tempo di costruire le immagini e si vedr&agrave;. Tutti bug [1], la schedule [3] e il log del meeting [2] sono riportati qui per completezza:</p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">[1]&nbsp;</span><a href="http://qa.fedoraproject.org/blockerbugs/milestone/21/beta/buglist" style="color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 13px;" target="_blank">http://qa.fedoraproject.org/<wbr />blockerbugs/milestone/21/beta/<wbr />buglist</a><br style="font-family: arial, sans-serif; font-size: 13px;" />
	<span style="font-family: arial, sans-serif; font-size: 13px;">[2]&nbsp;</span><a href="http://meetbot.fedoraproject.org/fedora-meeting-2/2014-10-23/f21_beta_gono-go_meeting.2014-10-23-17.00.html" style="color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 13px;" target="_blank">http://meetbot.fedoraproject.<wbr />org/fedora-meeting-2/2014-10-<wbr />23/f21_beta_gono-go_meeting.<wbr />2014-10-23-17.00.html</a><br style="font-family: arial, sans-serif; font-size: 13px;" />
	<span style="font-family: arial, sans-serif; font-size: 13px;">[3]&nbsp;</span><a href="https://fedoraproject.org/wiki/Releases/21/Schedule" style="color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 13px;" target="_blank">https://fedoraproject.org/<wbr />wiki/Releases/21/Schedule</a></p>
