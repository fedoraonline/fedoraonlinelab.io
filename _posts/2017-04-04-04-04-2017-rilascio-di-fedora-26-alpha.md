---
categories:
- staff news
layout: ultime_news
title: '04/04/2017: Rilascio di Fedora 26 Alpha!'
created: 1491331653
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2017/04/f26-alpha.png-945x400.jpg" style="width: 295px; height: 125px;" /></p>
<p>&nbsp;</p>
<p>Come da titolo, la milestone Alpha di Fedora 26 &egrave; stata rilasciata nel corso della giornata odierna!<br />
	<br />
	<br />
	<strong>Link utili</strong>:<br />
	Articolo Fedora magazine: <a href="https://fedoramagazine.org/fedora-26-alpha-available-now/">https://fedoramagazine.org/fedora-26-alpha-available-now/</a><br />
	Lista dei cambiamenti previsti per Fedora 26: <a href="https://fedoraproject.org/wiki/Releases/26/ChangeSet">https://fedoraproject.org/wiki/Releases/26/ChangeSet</a><br />
	Link per il download di Fedora Workstation: <a href="https://getfedora.org/workstation/prerelease/">https://getfedora.org/workstation/prerelease/</a><br />
	Link per il download di Fedora Server: <a href="https://getfedora.org/server/prerelease/">https://getfedora.org/server/prerelease/</a><br />
	Link per il download dei labs di Fedora: <a href="https://spins.fedoraproject.org/prerelease">https://spins.fedoraproject.org/prerelease</a><br />
	Link per il download delle spins di Fedora: <a href="https://spins.fedoraproject.org/prerelease">https://spins.fedoraproject.org/prerelease</a><br />
	Link per il download di Fedora ARM: <a href="https://arm.fedoraproject.org/prerelease">https://arm.fedoraproject.org/prerelease</a><br />
	&nbsp;</p>
