---
categories:
- staff news
layout: ultime_news
title: 'Fedora 24 Alpha è GO: verrà rilasciata il 29 Marzo 2016!'
created: 1458769896
---
<p class="rtejustify"><img alt="Desktop di Fedora 24 Alpha" src="http://juliux.altervista.org/images/desktops/fedora/f24_alpha.png" style="width: 400px; height: 225px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Nel corso del secondo Go/No-Go Meeting, relativo a Fedora 24 Alpha, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.</span></p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Fedora 24 Alpha sar&agrave; quindi pubblicamente disponibile il giorno 29 Marzo 2016, alle ore 16:00 italiane.</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting/2016-03-23/f24-alpha-go_no_go-meeting.2016-03-23-17.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting/2016-03-23/f24-alpha-go_no_go-meeting.2016-03-23-17.00.log.html</a></span></p>
<p class="rtejustify">&nbsp;</p>
