---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 20 Beta
created: 1384268851
---
<p>E&#39; appena stata rilasciata la Beta di Fedora 20, ultima test release prima del rilscio finale il 10 dicembre (si spera).</p>
<p>Le maggiori novit&agrave; e l&#39;annuncio ufficiale si possono leggere qui:</p>
<p><a href="https://lists.fedoraproject.org/pipermail/announce/2013-November/003184.html">https://lists.fedoraproject.org/pipermail/announce/2013-November/003184.html</a></p>
<p>Buon testing a tutti!</p>
