---
categories:
- staff news
layout: ultime_news
title: FUDCon Paris 2012 - 13/15 ottobre
created: 1341220308
---
<p>
	Ci siamo!</p>
<p>
	Dopo il FUDCon di Milano &egrave; l&#39;anno di Parigi, un incontro molto interessante della durata di 3 giorni in cui si alternano talk, workshops e discussioni di contributor selezionati su temi specifici. Il tutto condito da aperitivi, cene in cui socializzare con altri contributor e gadget di ricordo di questo evento molto apprezzato.<br />
	Sono iniziate le iscrizioni e come di consueto &egrave; stato messo a disposizione, dai colleghi francesi, un albergo apposito a condizioni particolari. <a href="http://www.ibishotel.com/gb/hotel-1401-ibis-paris-la-villette-cite-des-sciences-19eme/index.shtml">http://www.ibishotel.com/gb/hotel-1401-ibis-paris-la-villette-cite-des-sciences-19eme/index.shtml</a></p>
<p>
	La prenotazione &egrave; molto semplice, inviando la mail all&#39;albergo si dovr&agrave; specificare:</p>
<pre>
* Nome e Cognome
* Telefono
* Numero carta di credito e data di scadenza (si pu&ograve; chiedere anche metodi alternativi)
* Aggiornare la tabella: <a href="https://fedoraproject.org/wiki/FUDCon:Paris_2012#Pre-registration">https://fedoraproject.org/wiki/FUDCon:Paris_2012#Pre-registration</a>
* Dite all&#39;albergo che fate parte del &quot;Fedora Group&quot;</pre>
<p>
	Nulla &egrave; d&#39;obbligo, chi vuole pu&ograve; anche pernottare altrove o da amici per chi ne ha nella capitale francese. E&#39; possibile dividere la stanza con altri contributor, come &egrave; altrettanto possibile prenotare una singola. Il wifi &egrave; gratuito. Tutti i dettagli e la pagina wiki in cui registrarsi, lasciando anche la misura per la maglietta del FUDCon, si trova qui:<br />
	<a href="https://fedoraproject.org/wiki/FUDCon:Paris_2012">https://fedoraproject.org/wiki/FUDCon:Paris_2012</a> Altre informazioni sull&#39;evento, sui costi per chi arriva da altri parti del mondo e sulla citt&agrave; potete trovarli qui:<br />
	<a href="https://fedoraproject.org/wiki/FUDCon:Bid_for_Paris_2012">https://fedoraproject.org/wiki/FUDCon:Bid_for_Paris_2012</a><br />
	Dall&#39;Italia invece &egrave; consigliabile prenotare il volo gi&agrave; adesso, si trova a prezzi molto convenienti. Per chi abita nel Nord-Ovest si pu&ograve; pensare anche al treno, da poco esiste anche una tariffa economica del TGV.<br />
	Per qualsiasi informazione sull&#39;evento e sulla prenotazione ci si pu&ograve; rivolgere a:</p>
<pre>
<a href="https://admin.fedoraproject.org/mailman/listinfo/fudcon-planning">fudcon-emea at fedoraproject.org</a></pre>
