---
categories:
- staff news
layout: ultime_news
title: Nuove Fedora 18 live-respin
created: 1365060872
---
<p>Per chi &egrave; interessato, ed &egrave; utile per chi vuole fare un&#39;installazione di Fedora 18, sono disponibili nuove live-respin per Gnome, KDE, XFCE e Lxde. Le immagini sono aggiornate al 26 marzo.</p>
<p><a href="http://alt.fedoraproject.org/pub/alt/live-respins/">http://alt.fedoraproject.org/pub/alt/live-respins/</a></p>
<p>Una respin &egrave; un&#39;immagine ISO&nbsp;ricreata con gli ultimi aggiornamenti ufficiali di Fedora, quindi un&#39;installazione da queste immagini parte direttamente con un kernel 3.8.x e non ci si trover&agrave; di fronte a&nbsp; 500MB di aggiornamenti&nbsp;o pi&ugrave;, come invece succede con la ISO del 15 gennaio (ovviamente).</p>
<p>Le respin sono utili e comodi, ma &egrave; da sottolineare che comunque non si tratta di immagini ufficiali, ma ufficiosi. Se si vuole un&#39;immagine ufficiale esiste una sola, ovvero una tra&nbsp;quelle disponibili sul sito <a href="http://fedoraproject.org">http://fedoraproject.org</a></p>
