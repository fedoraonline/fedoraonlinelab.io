---
categories:
- staff news
layout: ultime_news
title: Fedora 24 ha raggiunto l'End of Life
created: 1504789070
---
<p><img alt="" src="https://fedoramagazine.org/wp-content/uploads/2017/07/fedora24eol-945x400.jpg" style="width: 399px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Ricordiamo all&#39;utenza che, nel corso della giornata del <strong>08 Agosto 2017</strong>, il rilascio 24 di Fedora ha raggiunto lo stato di <strong>End Of Life</strong>.</p>
<p class="rtejustify">Questo implica che, alla data indicata:</p>
<ul>
	<li class="rtejustify">
		I pacchetti nei repository di Fedora 24, non riceveranno pi&ugrave; aggiornamenti (neanche quelli pi&ugrave; importanti, relativi alla sicurezza).</li>
	<li class="rtejustify">
		Nessun nuovo software verr&agrave; aggiunto ai repository di Fedora 24.</li>
	<li class="rtejustify">
		&Egrave; consigliato l&#39;upgrade verso una versione attivamente mantenuta (Fedora 25 o Fedora 26).</li>
</ul>
<p class="rtejustify">A tal proposito, Fedora Online mette a disposizione alcune guide, ancora sostanzialmente valide:</p>
<ul>
	<li class="rtejustify">
		Installazione pulita di Fedora 26 (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>).</li>
	<li class="rtejustify">
		Avanzamento di versione da Fedora 24 a Fedora 25 o 26 (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>).</li>
</ul>
