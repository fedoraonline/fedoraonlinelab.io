function removeCDATA(string) {
  return string.replace("<![CDATA[", "").replace("]]>", "");
}

let containers = document.getElementsByClassName('rss');
for (let i=0; i<containers.length; i++) {
  let locale = containers[i].dataset.locale;
  let limit = containers[i].dataset.limit;
  fetch(containers[i].dataset.src).then((res) => {
    res.text().then((xmlTxt) => {
      let domParser = new DOMParser();
      let feed = domParser.parseFromString(xmlTxt, 'text/xml');
      let items = feed.getElementsByTagName('item');
      let maximum = items.length>limit ? limit : items.length;
      for (let j=0; j<maximum; j++) {
        let title = removeCDATA(items[j].querySelector('title').innerHTML);
        let link = items[j].querySelector('link').innerHTML;
        let author = items[j].querySelector('author').innerHTML.match(/\((.*)\)/)[1];
        let date = new Date(items[j].querySelector('pubDate').innerHTML).toLocaleString(locale);
        let row = document.createElement('tr');
        row.innerHTML = `<td>${date}</td><td><a href="${link}">${title}</a></td><td>${author}</td>`;
        containers[i].appendChild(row);
      }
    });
  }).catch(() => containers[i].innerHTML = "<i>Unable to fetch RSS feed</i>");
}

